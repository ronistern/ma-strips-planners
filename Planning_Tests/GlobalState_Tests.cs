﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Planning.planners;
namespace Planning_Tests
{
    [TestFixture]
    public class GlobalState_Tests
    {
        [Test]
        public void TestEquals()
        {
            //<todo> DO THIS </todo>

            Dictionary<string, string> dict1 = new Dictionary<string, string>();
            Dictionary<string, string> dict2 = new Dictionary<string, string>();

            dict1.Add("key1", "value1");
            dict1.Add("key2", "value2");

            dict2.Add("key2", "value2");
            Assert.AreNotEqual(dict1, dict2);

            dict2.Add("key1", "value1");
            Assert.AreEqual(dict1, dict2);
        }

        [Test]
        public void TestHashCode()
        {
            Dictionary<string, string> dict1 = new Dictionary<string, string>();
            Dictionary<string, string> dict2 = new Dictionary<string, string>();

            dict1.Add("key1", "value1");
            dict1.Add("key2", "value2");

            dict2.Add("key2", "value2");
            dict2.Add("key1", "value1");
            Assert.AreEqual(dict1.GetHashCode(), dict2.GetHashCode());
        }
    }
}
