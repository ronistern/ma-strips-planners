﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planning.planners
{
    abstract class AbstractPlanner
    {
        protected ProblemSolver solver;
        protected MaStripsProblem maStripsProblem;

        //private HashSet<Constant> publicConst;
        //private List<Domain> domains; // Grounded domains
        //private Dictionary<string, HashSet<string>> mapActionNameToAgents; // Map action name to agents
        public AbstractPlanner(ProblemSolver solver)
        {
            this.solver = solver;
        }



        /// <summary>
        /// Fundamental multi-agent planning cycle (a simulation of a parallel execution)
        /// </summary>
        /// <param name="agents"></param>
        /// <returns></returns>
        public List<string> Plan(MaStripsProblem maStripsProblem)
        {
            // Create agents
            Setup(maStripsProblem);
            List<AbstractPlanningAgent> agents = CreateAgents();
            
            // Plan 
            bool solved = false;
            while (true)
            {
                foreach (AbstractPlanningAgent agent in agents)
                {
                    solved = agent.SinglePlanningIteration();
                    if(solved)
                        return ConstructPlan(agents);
                }
                // <todo> handle cases where there is no solution </todo>
            }
        }

        /// <summary>
        /// Setup the planner for solving the given MA-STRIPS problem. This is the place for preprocessing
        /// </summary>
        /// <param name="maStripsProblem"></param>
        protected void Setup(MaStripsProblem maStripsProblem)
        {
            this.maStripsProblem = maStripsProblem;
        }

        /// <summary>
        /// Create the team of agents for solving this problem (this.maStripsProblem).
        /// </summary>
        /// <returns></returns>
        protected abstract List<AbstractPlanningAgent> CreateAgents();

        /// <summary>
        /// The team returns the found plan. 
        /// NOTE: This method should be called AFTER planning.  
        /// </summary>
        /// <param name="agents"></param>
        /// <returns></returns>
        public abstract List<string> ConstructPlan(List<AbstractPlanningAgent> agents);




        /// Utility methods        
        protected bool IsPrivateAction(Action action)
        {
            foreach (Domain domain in this.maStripsProblem.domains)
            {
                if(domain.PublicActions.Contains(action))
                    return false;
            }
            return true;
        }
    }
}
