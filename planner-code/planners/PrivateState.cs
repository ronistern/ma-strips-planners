﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planning.planners
{
    class PrivateState
    {
        public HashSet<Predicate> privatePredicates;

        public PrivateState(HashSet<Predicate> privatePredicates)
        {
            this.privatePredicates = privatePredicates;
        }
    }
}
