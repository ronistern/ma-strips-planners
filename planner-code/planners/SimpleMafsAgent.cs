﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planning.planners
{
    class SimpleMafsAgent : AbstractPlanningAgent
    {

        protected List<SearchNode> openList; // Nodes that are considered for expansions
        protected List<SearchNode> closedList; // Nodes that were already expanded
        protected HashSet<PrivateState> privateStates; // List of private states. This is used to avoid duplicate private states

        private SearchNode incumbentGoal;
        private int expanded;

        public SimpleMafsAgent(Problem problem, Domain domain, List<AbstractPlanningAgent> agentTeam)
            : base(problem, domain, agentTeam)
        {}

        /// <summary>
        /// Set the start and goal states and prepare to start planning
        /// </summary>
        /// <param name="initialState"></param>
        /// <param name="goalState"></param>
        override public void Setup(GlobalState initialState, GlobalState goalState)
        {
            base.Setup(initialState, goalState);
            this.incumbentGoal = null;
            this.openList = new List<SearchNode>();
            this.closedList = new List<SearchNode>();
            this.privateStates = new HashSet<PrivateState>();
            this.openList.Add(new SearchNode(initialState, null, 0, null));
            this.expanded = 0;
        }


        /// <summary>
        /// Perform a single planning iteration. 
        /// <todo> Better explain the concept of iteration </todo>
        /// </summary>
        /// <returns>True, iff a goal is found</returns>
        public override bool SinglePlanningIteration()
        {
            this.ProcessInbox(); // Process all the sent messages

            bool goalFound = false;
            if (this.openList.Count > 0)
            {
                SearchNode currentNode = PopBest();
                goalFound = Expand(currentNode);
                this.expanded++;
            }
            else
            {
                Console.WriteLine("Agent " + this.agentId + " Idle");
            }
            return goalFound;
        }

        /// <summary>
        /// Process the messages in the inbox and clear it to make room for new ones
        /// </summary>
        private void ProcessInbox()
        {
            lock (this.inbox)
            {
                foreach (Message message in this.inbox)
                {
                    if (message.content is SearchNode)
                    {
                        PlanningLogger.Log("\t\t\t\t Recieved newNode from " + message.sender.agentId);
                        SearchNode newNode = (SearchNode)message.content;
                        if (this.DuplicateDetection(newNode) == false)
                            this.openList.Add(newNode);
                    }
                }
                this.inbox.Clear();
            }
        }

        /// <summary>
        /// Chooses the next global state to expand
        /// </summary>
        /// <returns></returns>
        protected SearchNode PopBest()
        {
            /// **** BASELINE IMPLEMENTATIONS: FIFO *** ///
            SearchNode currentNode = this.openList.ElementAt(0);
            this.openList.Remove(currentNode);
            this.closedList.Add(currentNode);
            return currentNode;
        }


        /// <summary>
        /// Expand the current state, and return true if one of the generated states 
        /// may be a goal state.
        /// </summary>
        /// <param name="currentState"></param>
        /// <returns></returns>
        protected bool Expand(SearchNode currentNode)
        {
            bool goalFound=false;
            GlobalState currentState = currentNode.state;
            GlobalState generatedState;
            SearchNode newSearchNode;
            foreach (Action action in GetPossibleActions(currentState))
            {
                // Apply action
                generatedState = this.Apply(action, currentState);
                newSearchNode = new SearchNode(generatedState, currentNode, currentNode.g + action.cost, action);

                PlanningLogger.Log(this.expanded+"\t Agent " + this.agentId+ "\t Applied " + action.Name
                    + "\t Generated " + generatedState + "\t g=" + newSearchNode.g);

                PlanningLogger.Log(generatedState.ToLog());

                if (DuplicateDetection(newSearchNode) == false)
                {
                    // Goal test
                    if (IsGoal(generatedState))
                    {
                        goalFound = true;

                        // Keep track of the best goal seen so far (the incumbent solution)
                        if (this.incumbentGoal == null)
                            this.incumbentGoal = newSearchNode;
                        else if (this.incumbentGoal.g > newSearchNode.g)
                            this.incumbentGoal = newSearchNode;
                    }
                    else
                    {
                        this.openList.Add(newSearchNode);

                        // Send state to all other agents if action is public
                        if (this.domain.PublicActions.Contains(action))
                        {
                            PlanningLogger.Log("\t\t\t\t Broadcasting the generated node");
                            foreach (AbstractPlanningAgent agent in this.agentTeam)
                                if (this != agent)
                                    this.Send(newSearchNode, agent);
                        }
                    }
                }
            }

            return goalFound;
        }

        /// <summary>
        /// Checks if the given state is a goal state
        /// <todo> Replace this with a distributed check, asking all agents if they agree on the goal </todo>
        /// </summary>
        /// <param name="candidate">A global state that may be a goal state</param>
        /// <returns>True iff candidate is goal</returns>
        public bool IsGoal(GlobalState candidate)
        {
            return this.goalState.IsTrue(candidate);
            // return this.goalState.Equals(candidate); The goal state may not be a complete state
        }


        /// <summary>
        /// Checks if the given global state already exists in the open and closed list
        /// Also, updates g-value if needed.
        /// CURRENT IMPLEMENTATION DOES NOT REOPEN CLOSED NODES
        /// </summary>
        /// <param name="newGlobalState"></param>
        /// <returns>true if newGlobalState is already in the open or closed list</returns>
        protected bool DuplicateDetection(SearchNode newSearchNode)
        {
            /// <todo> Implement duplicate detection for private states to save space 
            // PrivateState newPrivateState = newGlobalState.agentToPrivateState[this];
            //if (this.privateStates.Contains(newPrivateState))
            //{
            //    newPrivateState = this.privateStates.Get(newPrivateState);
            //    newGlobalState[this] = newPrivateState;
            //}

            // Check if the global state already exists
            GlobalState newGlobalState = newSearchNode.state;
            SearchNode duplicate = null;
            if ((this.openList.Contains(newSearchNode)) || (this.closedList.Contains(newSearchNode)))
            {
                int duplicateIndex = this.openList.IndexOf(newSearchNode);
                duplicate = this.openList.ElementAt(duplicateIndex);
                // If found a better path to this state, update g and parent
                if (newSearchNode.g < duplicate.g)
                {
                    duplicate.g = newSearchNode.g;
                    duplicate.parent = newSearchNode.parent;
                    duplicate.generatingAction = newSearchNode.generatingAction;
                }
                return true;
            }
            return false;
        }


        /// <summary>
        /// Return the current best solution found, in the form of a list of actions transitioning the start state to a goal state.
        /// <todo> Implepment this in a distributed, privacy preserving, way </todo>
        /// </summary>
        /// <returns></returns>
        public List<Action> GetIncumbentSolution()
        {
            if (this.incumbentGoal == null) return null;

            List<Action> solution = new List<Action>();
            SearchNode current = this.incumbentGoal;
            
            while(current.generatingAction!=null)
            {
                solution.Insert(0,current.generatingAction);
                current = current.parent;
            }
            return solution;
        }

        /// <summary>
        /// A search node represents a state and additional information needed to guide the planning algorithm.
        /// </summary>
        protected class SearchNode
        {
            public GlobalState state;
            public SearchNode parent;
            public Action generatingAction; // The action used to generated this state from the parent node
            public int g;
            public SearchNode(GlobalState state, SearchNode parent, int g, Action generatingAction)
            { 
                this.g = g; 
                this.state = state; 
                this.parent = parent; 
                this.generatingAction = generatingAction; 
            }

            public override bool Equals(object obj)
            { return state.Equals(((SearchNode)obj).state); }

            public override int GetHashCode()
            {   return state.GetHashCode(); }

            public string ToLog()
            {   return "State: " + state + ", g: " + g; }
        }  
    }
}
