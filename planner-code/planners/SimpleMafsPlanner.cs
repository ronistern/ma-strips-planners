﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Planning.planners
{
    class SimpleMafsPlanner : AbstractPlanner
    {
        public SimpleMafsPlanner(ProblemSolver solver) : base(solver)
        {}

        /// <summary>
        /// Create the team of agents for solving this problem (this.maStripsProblem).
        /// </summary>
        /// <returns></returns>
        override protected List<AbstractPlanningAgent> CreateAgents()
        {
            List<AbstractPlanningAgent> agentTeam = new List<AbstractPlanningAgent>();
            
            HashSet<Predicate> initPublicPredicates = new HashSet<Predicate>();
            HashSet<Predicate> initPrivatePredicates;
            Dictionary<AbstractPlanningAgent, PrivateState> initAgentToPrivateState 
                = new Dictionary<AbstractPlanningAgent, PrivateState>();

            HashSet<Predicate> goalPublicPredicates = new HashSet<Predicate>();
            HashSet<Predicate> goalPrivatePredicates;
            Dictionary<AbstractPlanningAgent, PrivateState> goalAgentToPrivateState
                = new Dictionary<AbstractPlanningAgent, PrivateState>();


            PrivateState privateState;
            AbstractPlanningAgent newAgent;            
            Problem problem;
            Domain domain;

            for (int i = 0; i < this.maStripsProblem.domains.Count; i++)
            {
                problem = this.maStripsProblem.problems[i];
                domain = this.maStripsProblem.domains[i];

                // Create an agent
                newAgent = this.CreateAgent(problem, domain, agentTeam);
                agentTeam.Add(newAgent);

                // Add its initial state to the global initial state
                initPrivatePredicates = new HashSet<Predicate>();
                foreach (Predicate predicate in problem.Known)
                {
                    if (domain.IsPublic(predicate))
                        initPublicPredicates.Add(predicate);
                    else
                        initPrivatePredicates.Add(predicate);
                    
                }
                privateState = new PrivateState(initPrivatePredicates);
                initAgentToPrivateState.Add(newAgent, privateState);

                // Add its goal state to the global goal state
                goalPrivatePredicates = new HashSet<Predicate>();
                foreach (Predicate predicate in problem.Goal.GetAllPredicates())
                {
                    if (domain.IsPublic(predicate))
                        goalPublicPredicates.Add(predicate);
                    else
                        goalPrivatePredicates.Add(predicate);
                }
                privateState = new PrivateState(goalPrivatePredicates);
                goalAgentToPrivateState.Add(newAgent, privateState);
            }
            
            // Create the initial and goal global state
            GlobalState initialState = new GlobalState(initPublicPredicates, initAgentToPrivateState);
            GlobalState goalState = new GlobalState(goalPublicPredicates, goalAgentToPrivateState);

            // Setup the agents with the initial and goal state
            foreach (AbstractPlanningAgent agent in agentTeam)
                agent.Setup(initialState, goalState);

            return agentTeam;
        }

        private AbstractPlanningAgent CreateAgent(Problem problem, Domain domain, List<AbstractPlanningAgent> agentTeam)
        {
            return new SimpleMafsAgent(problem, domain, agentTeam);
        }


        /// <summary>
        /// Construct a plan for the given set of agents. 
        /// This is done by finding the first agent that has a search node that is a goal, 
        /// and following backpointers from it to the start. 
        /// </summary>
        /// <param name="agents"></param>
        /// <returns></returns>
        override public List<string> ConstructPlan(List<AbstractPlanningAgent> agents)
        {
            SimpleMafsAgent mafsAgent;
            List<Action> solution;
            foreach (AbstractPlanningAgent agent in agents)
            {
                mafsAgent = (SimpleMafsAgent)agent;
                solution = mafsAgent.GetIncumbentSolution();
                if (solution != null)
                {
                    PlanningLogger.Log("Agent " + mafsAgent.agentId + " Found a solution!");
                    List<string> plan = new List<string>();
                    foreach (Action action in solution)
                    {
                        plan.Add(action.Name);
                        PlanningLogger.Log("Agent " + this.GetOwningAgent(action, agents).agentId + " does " + action.Name);
                    }
                    return plan;
                }
            }
            throw new Exception("Tried to ConstractPlan but no agent had a solution");
        }


        /// <summary>
        /// Return the agent from the given list of agents that can perform the given action, 
        /// i.e., it's one of the agent's public or private actions. 
        /// </summary>
        /// <param name="action"></param>
        /// <param name="agents"></param>
        /// <returns></returns>
        private AbstractPlanningAgent GetOwningAgent(Action action, List<AbstractPlanningAgent> agents)
        {
            foreach (AbstractPlanningAgent agent in agents)
            {
                if (agent.HasAction(action))
                    return agent;
            }
            return null;
        }

        /////////////////////////////////////////////


        ///// <summary>
        ///// Create planning agent objects from the given ungrounded domains and problems
        ///// </summary>
        ///// <param name="domains">The view of the domain for each agent</param>
        ///// <param name="problems">The view of the current problem for each agent</param>
        ///// <returns></returns>
        //public List<AbstractPlanningAgent> CreateAgents(List<Domain> notGroundedDomains, List<Problem> problems)
        //{

        //    List<AbstractPlanningAgent> agents = new List<AbstractPlanningAgent>();

        //    this.domains = solver.GroundDomains(notGroundedDomains, problems);

        //    this.publicConst = ExtractPublicConstants(domains); // publicConst contains all public constants in all domains

        //    HashSet<Predicate> lAllGoals = new HashSet<Predicate>();
        //    HashSet<Predicate> lAllDividedGoals = new HashSet<Predicate>();

        //    for (int i = 0; i < domains.Count; i++)
        //        agents.Add(CreateAgent(problems.ElementAt(i), domains.ElementAt(i), notGroundedDomains.ElementAt(i), i));

        //    return agents;
        //}

        ///// <summary>
        ///// Create single agent from the given problem and domain description. 
        ///// </summary>
        ///// <param name="p"></param>
        ///// <param name="d">Grounded domain description (from a single agent prespective)</param>
        ///// <param name="notGroundedDomain"></param>
        ///// <returns></returns>
        //public AbstractPlanningAgent CreateAgent(Problem p, Domain d, Domain notGroundedDomain, int agentIndex)
        //{
        //    // Add all public constants to the ungrounded domains
        //    foreach (Constant cons in publicConst)
        //    {
        //        if (!notGroundedDomain.Constants.Contains(cons))
        //        {
        //            notGroundedDomain.Constants.Add(cons);
        //        }
        //    }


        //    // Prepare the agent's domain
        //    HashSet<GroundedPredicate> publicPredicates = new HashSet<GroundedPredicate>();
        //    HashSet<GroundedPredicate> Predicates = new HashSet<GroundedPredicate>();
        //    PrepareDomain(d, publicPredicates, Predicates);


        //    // Prepare the agent's initial state
        //    State initialState = new State(p);
        //    foreach (Predicate gp in p.Known)
        //        if (gp is GroundedPredicate)
        //            initialState.AddPredicate(gp);

        //    // Prepare the goal state
        //    List<GroundedPredicate> goal = PrepareGoalState(p, d, publicPredicates, initialState);

        //    // Additional bookkeeping <todo> VERIFY IF NEEDED </todo>
        //    List<Action> projPublicActions = ExtractProjectedPublicActions(d, notGroundedDomain);
        //    Dictionary<Predicate, HashSet<Predicate>> lInvariants = ExtractInvariants(d, projPublicActions);
        //    UpdateActionNameMap(d, agentIndex);

        //    return new Agent(p, notGroundedDomain, d.groundedAction, d.PublicActions.ToList(),
        //        d.PrivateActions.ToList(), Predicates, publicPredicates, initialState, goal, "Agent: " + agentIndex, projPublicActions, lInvariants);
        //}

        ///// <summary>
        /////  Update the map that maps action names to the agent that can do them
        ///// </summary>
        ///// <param name="d"></param>
        //private void UpdateActionNameMap(Domain d, int agentIndex)
        //{
        //    if (mapActionNameToAgents == null)
        //        mapActionNameToAgents = new Dictionary<string, HashSet<string>>();

        //    foreach (Action act in d.groundedAction)
        //    {
        //        if (!mapActionNameToAgents.ContainsKey(act.Name))
        //            mapActionNameToAgents.Add(act.Name, new HashSet<string>());
        //        mapActionNameToAgents[act.Name].Add("Agent: " + agentIndex.ToString());
        //    }
        //}


        ///// <summary>
        ///// Extract the invariants in an agent's domain
        ///// </summary>
        ///// <param name="d"></param>
        ///// <param name="projPublicActions"></param>
        ///// <returns></returns>
        //private Dictionary<Predicate, HashSet<Predicate>> ExtractInvariants(Domain d, List<Action> projPublicActions)
        //{
        //    Dictionary<Predicate, HashSet<Predicate>> lInvariants = null;
        //    lInvariants = d.IdentifyStrongInvariants(projPublicActions);
        //    Dictionary<Predicate, HashSet<Predicate>> tmp = d.IdentifyInvariants(projPublicActions);
        //    foreach (var kv in tmp)
        //    {
        //        if (!lInvariants.ContainsKey(kv.Key))
        //        {
        //            lInvariants.Add(kv.Key, kv.Value);
        //        }
        //        else
        //        {
        //            foreach (GroundedPredicate gp in kv.Value)
        //            {
        //                lInvariants[kv.Key].Add(gp);
        //            }
        //        }
        //    }
        //    return lInvariants;
        //}


        ///// <summary>
        ///// Create a list of all public actions known to the agent. 
        ///// This will be the agent's public actions and a projected view of the public actions of the other agents.
        ///// <todo>verify</todo>
        ///// </summary>
        ///// <param name="d"></param>
        ///// <param name="notGroundedDomain"></param>
        ///// <returns></returns>
        //private List<Action> ExtractProjectedPublicActions(Domain d, Domain notGroundedDomain)
        //{
        //    List<Action> allPublicActions = new List<Action>();
        //    HashSet<Predicate> allPublicPredicate = new HashSet<Predicate>();

        //    allPublicActions.Union(d.PublicActions);

        //    foreach (Domain otherAgentDomain in this.domains)
        //    {
        //        if (!otherAgentDomain.Equals(d))
        //        {
        //            foreach (Action act in otherAgentDomain.PublicActions)
        //            {
        //                Action newAct = new Action(act.Name);
        //                CompoundFormula effect = new CompoundFormula("and");
        //                CompoundFormula preCond = new CompoundFormula("and");
        //                foreach (GroundedPredicate eff in act.HashEffects)
        //                {
        //                    if (allPublicPredicate.Contains(eff) || allPublicPredicate.Contains(eff.Negate()))
        //                        effect.AddOperand(eff);
        //                }
        //                foreach (GroundedPredicate pre in act.HashPrecondition)
        //                {
        //                    if (allPublicPredicate.Contains(pre) || allPublicPredicate.Contains(pre.Negate()))
        //                        preCond.AddOperand(pre);
        //                }

        //                newAct.Effects = effect;
        //                newAct.Preconditions = preCond;
        //                newAct.LoadPrecondition();
        //                allPublicActions.Add(newAct);
        //            }
        //        }
        //    }

        //    notGroundedDomain.Actions = new List<Action>(d.Actions);
        //    notGroundedDomain.Actions.AddRange(allPublicActions);
        //    allPublicActions.AddRange(d.groundedAction);
        //    return allPublicActions;
        //}




        ///// <summary>
        ///// Prepare the goal state predicates
        ///// </summary>
        ///// <param name="p"></param>
        ///// <param name="d"></param>
        ///// <param name="publicPredicates"></param>
        ///// <param name="initialState"></param>
        ///// <returns></returns>
        //private List<GroundedPredicate> PrepareGoalState(Problem p, Domain d, HashSet<GroundedPredicate> publicPredicates, State initialState)
        //{
        //    List<GroundedPredicate> goal = DivideGoal(p.Goal, d.PublicActions.ToList(), d.PrivateActions.ToList(), publicPredicates);
        //    HashSet<Predicate> lAllGoals = new HashSet<Predicate>();
        //    HashSet<Predicate> lAllDividedGoals = new HashSet<Predicate>();

        //    lAllGoals.UnionWith(p.Goal.GetAllPredicates());
        //    foreach (Predicate pGoal in goal)
        //        lAllDividedGoals.Add(pGoal);
        //    return goal;
        //}

        ///// <summary>
        ///// Prepare the agent's domain
        ///// <todo>VERIFY</todo>
        ///// </summary>
        ///// <param name="domain"></param>
        ///// <param name="publicPredicates"></param>
        ///// <param name="Predicates"></param>
        //private static void PrepareDomain(Domain domain, HashSet<GroundedPredicate> publicPredicates, HashSet<GroundedPredicate> Predicates)
        //{
        //    domain.Predicates = new HashSet<Predicate>();
        //    foreach (GroundedPredicate gp in domain.PublicPredicates)
        //    {
        //        publicPredicates.Add(gp);
        //        Predicates.Add(gp);
        //        GroundedPredicate ngp = (GroundedPredicate)gp.Negate();
        //        publicPredicates.Add(ngp);
        //        Predicates.Add(ngp);

        //        domain.Predicates.Add(gp);
        //        domain.Predicates.Add(ngp);
        //    }


        //    foreach (GroundedPredicate gp in domain.PrivatePredicates)
        //    {
        //        Predicates.Add(gp);
        //        GroundedPredicate ngp = (GroundedPredicate)gp.Negate();
        //        Predicates.Add(ngp);

        //        domain.Predicates.Add(gp);
        //        domain.Predicates.Add(ngp);
        //    }
        //    domain.groundedAction = domain.Actions;
        //}












        ///// <summary>
        ///// Divide the goal
        ///// <todo> VERIFY </todo>
        ///// </summary>
        ///// <param name="goal"></param>
        ///// <param name="publicActions"></param>
        ///// <param name="privateActions"></param>
        ///// <param name="publicFacts"></param>
        ///// <returns></returns>
        //private List<GroundedPredicate> DivideGoal(Formula goal, List<Action> publicActions, List<Action> privateActions, HashSet<GroundedPredicate> publicFacts)
        //{
        //    List<GroundedPredicate> localGoal = new List<GroundedPredicate>();

        //    foreach (GroundedPredicate gp in goal.GetAllPredicates())
        //    {
        //        bool flag = false;
        //        foreach (Action act in publicActions)
        //        {
        //            if (act.Effects.GetAllPredicates().Contains(gp))
        //            {
        //                // gp.isPublic = true;
        //                flag = true;
        //                localGoal.Add(gp);
        //                break;
        //            }
        //            if (act.Preconditions.GetAllPredicates().Contains(gp))
        //            {
        //                //gp.isPublic = true;
        //                flag = true;
        //                localGoal.Add(gp);
        //                break;
        //            }
        //        }
        //        if (flag != true)
        //        {
        //            foreach (Action act in privateActions)
        //            {
        //                if (act.Effects.GetAllPredicates().Contains(gp))
        //                {
        //                    flag = true;
        //                    localGoal.Add(gp);
        //                    break;
        //                }
        //            }
        //        }
        //        if (flag)
        //        {
        //            if (publicFacts.Contains(gp))
        //            {
        //                gp.isPublic = true;
        //            }
        //        }
        //    }
        //    return localGoal;
        //}








        ///// <summary>
        ///// //////////////////////////////////////
        ///// </summary>
        ///// <param name="domains"></param>
        ///// <returns></returns>


        //private static HashSet<Constant> ExtractPublicConstants(List<Domain> domains)
        //{
        //    HashSet<Constant> publicConst = new HashSet<Constant>();
        //    for (int i = 0; i < domains.Count; i++)
        //    {
        //        foreach (Constant cons in domains.ElementAt(i).PublicConstants)
        //        {
        //            publicConst.Add(cons);
        //        }
        //    }
        //    return publicConst;
        //}

        //public List<AbstractPlanningAgent> BuildAgents(List<Domain> lDomains, List<Problem> lProblems)
        //{

        //    PrepareAgentsGoals(agents);
        //    foreach (Agent agent in agents)
        //    {
        //        agent.InitMutex();
        //    }

        //    agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

        //    Console.WriteLine("Planning");


        //    List<GroundedPredicate> allGoals = new List<GroundedPredicate>();
        //    Dictionary<string, int> indexStates = new Dictionary<string, int>();

        //    // Publish all  goals
        //    foreach (Agent agent in agents)
        //    {
        //        allGoals = allGoals.Union(agent.goal).ToList();
        //        sendedToAllSign.Add(agent.name);

        //    }
        //    HashSet<GroundedPredicate> allG = new HashSet<GroundedPredicate>(allGoals);
        //    foreach (Agent agent in agents)
        //    {
        //        agent.initPlaner();
        //        PreparePublicActions(agent);

        //        HashSet<GroundedPredicate> preconditions = agent.GetAllPublicPreconditions();
        //        agentsPublicPreconditions.Add(agent, preconditions);
        //        agentsPublicEffects.Add(agent, agent.GetAllPublicEffects());
        //        indexStates.Add(agent.name, 0);

        //        receivedStates.Add(agent.name, new HashSet<MafsVertex>());
        //        openLists.Add(agent.name, new HashSet<MafsVertex>());
        //        mutexs.Add(agent.name, new Mutex());
        //        countOfLandmarks.Add(agent.name, agent.publicRelevantLandmark.Count);
        //        countOfReasenOrders.Add(agent.name, agent.ReasonableOrdering.Count);

        //        foreach (GroundedPredicate pGp in agent.PublicPredicates)
        //        {
        //            allPublicFacts.Add(pGp);
        //        }
        //    }
        //    MafsVertex.init(agents);
        //    List<GroundedPredicate> fullStart = new List<GroundedPredicate>();
        //    foreach (Agent a in agents)
        //    {
        //        foreach (GroundedPredicate gp in a.startState.m_lPredicates)
        //        {
        //            fullStart.Add(gp);
        //        }
        //    }
        //    HashSet<GroundedPredicate> publicStartState = new HashSet<GroundedPredicate>();
        //    Dictionary<string, State> agPriv = new Dictionary<string, State>();
        //    foreach (Agent a in agents)
        //    {
        //        State privateStartState = new State((Problem)null);

        //        foreach (GroundedPredicate gp in a.startState.m_lPredicates)
        //        {
        //            if (!a.PublicPredicates.Contains(gp))
        //                privateStartState.AddPredicate(gp);
        //            else
        //            {
        //                publicStartState.Add(gp);
        //            }
        //        }
        //        agPriv.Add(a.name, privateStartState);

        //    }
        //    List<MafsVertex> lVertexes = new List<MafsVertex>();
        //    Dictionary<string, MafsAgent> MapsAgents = new Dictionary<string, MafsAgent>();
        //    this.MapsAgents = new List<MafsAgent>();
        //    foreach (Agent a in agents)
        //    {
        //        MafsVertex agentStartVertex = new MafsVertex(publicStartState, agPriv[a.name], indexStates, countOfLandmarks, countOfLandmarks.Keys.ToArray(), a.name, a.allGoals.Count, countOfReasenOrders);
        //        MafsAgent mAgent = new MafsAgent(agentStartVertex, a, a.allGoals, countOfLandmarks, openLists, receivedStates, mutexs, countOfReasenOrders, fullStart);
        //        mAgent.projectionHeuristic = new AdvancedProjectionHeuristic(a, agents, lDomains, lProblems);
        //        mAgent.SetPrivateState(agPriv[a.name]);
        //        MapsAgents.Add(mAgent.name, mAgent);
        //        this.MapsAgents.Add(mAgent);
        //        lVertexes.Add(agentStartVertex);
        //    }
        //    MafsVertex.UpDateAgents(MapsAgents);
        //    foreach (MafsVertex v in lVertexes)
        //    {
        //        v.PreaperFirstVertex();
        //    }
        //    foreach (Agent a in agents.ToList())
        //    {
        //        recoverActionEffect.Add(a.name, new Dictionary<string, HashSet<string>>());
        //        foreach (Action act in a.publicActions)
        //        {
        //            recoverActionEffect[a.name].Add(act.Name, new HashSet<string>());
        //            bool breaking = false;
        //            foreach (GroundedPredicate eff in act.HashEffects)
        //            {
        //                if (eff.Negation)
        //                {
        //                    foreach (Agent a2 in agents.ToList())
        //                    {
        //                        if (a2.name.Equals(a.name))
        //                            continue;
        //                        HashSet<GroundedPredicate> agentEff = agentsPublicEffects[a2];
        //                        if (agentEff.Contains((GroundedPredicate)eff.Negate()))
        //                        {
        //                            recoverActionEffect[a.name][act.Name].Add(a2.name);
        //                            breaking = true;
        //                            break;
        //                        }
        //                    }
        //                    if (breaking)
        //                        break;
        //                }
        //            }

        //        }
        //        AgentToInfluActions.Add(a.name, new Dictionary<string, HashSet<Action>>());
        //        foreach (Action act in a.publicActions)
        //        {
        //            if (!actionMap.ContainsKey(act.Name))
        //                actionMap.Add(act.Name, new HashSet<string>());
        //            foreach (var pair in agentsPublicPreconditions)
        //            {
        //                //  if (a.Equals(pair.Key))
        //                //    continue;
        //                bool flag = false;
        //                foreach (GroundedPredicate eff in act.HashEffects)
        //                {
        //                    if (pair.Value.Contains(eff))
        //                    {
        //                        flag = true;
        //                        break;
        //                    }
        //                }
        //                if (flag)
        //                {
        //                    actionMap[act.Name].Add(pair.Key.name);
        //                }
        //            }
        //        }
        //        influencedByAgents.Add(a.name, new List<string>());
        //        foreach (Agent a2 in agents.ToList())
        //        {
        //            AgentToInfluActions[a.name].Add(a2.name, new HashSet<Action>());
        //            foreach (Action act in a.publicActions)
        //            {

        //                bool effFlag = false;
        //                foreach (GroundedPredicate eff in act.HashEffects)
        //                {
        //                    if (agentsPublicPreconditions[a2].Contains(eff))
        //                    {
        //                        effFlag = true;
        //                        break;
        //                    }
        //                }

        //                if (effFlag)
        //                {
        //                    AgentToInfluActions[a.name][a2.name].Add(act);
        //                }
        //            }


        //            if (a.name.Equals(a2.name))
        //                continue;
        //            bool flag = false;
        //            foreach (GroundedPredicate pre in agentsPublicPreconditions[a])
        //            {
        //                if (agentsPublicEffects[a2].Contains(pre))
        //                {
        //                    influencedByAgents[a.name].Add(a2.name);
        //                    break;
        //                }
        //            }
        //        }
        //    }

        //}

        ///// <summary>
        ///// Prepare data structures relevant for the public actions
        ///// </summary>
        ///// <param name="agent"></param>
        //private void PreparePublicActions(Agent agent)
        //{
        //    foreach (Action publicAction in agent.publicActions)
        //    {
        //        // Mark the actions that have an effect that deletes a public predicate
        //        foreach (GroundedPredicate effect in publicAction.HashEffects)
        //        {
        //            if ((agent.PublicPredicates.Contains(effect) ||
        //                agent.PublicPredicates.Contains((GroundedPredicate)effect.Negate()))
        //                && effect.Negation)
        //            {
        //                publicAction.isDeletePublic = true;
        //                break;
        //            }
        //        }


        //        // Set prevail preconditions as effects (<todo> ask shlomi why </todo> )
        //        foreach (GroundedPredicate precondition in publicAction.HashPrecondition)
        //        {
        //            if (agent.PublicPredicates.Contains(precondition))
        //            {
        //                if (publicAction.HashEffects.Contains((GroundedPredicate)precondition.Negate()))
        //                    continue;
        //                ((CompoundFormula)publicAction.Effects).AddOperand(precondition);
        //                publicAction.HashEffects.Add(precondition);
        //            }

        //        }
        //    }
        //}

        ///////////////////////////////////////////

       // public int generateCounter = 0;
       // public bool stop = false;
       // public int massageEffCounter = 0;
       // public int massagePreCounter = 0;
       // public HashSet<GroundedPredicate> allPublicFacts = null;
       // List<MafsAgent> MapsAgents = null;
       // Dictionary<string, HashSet<MafsVertex>> openLists = null;
       // Dictionary<string, HashSet<MafsVertex>> receivedStates = null;
       // Dictionary<string, Mutex> mutexs = null;
       // Dictionary<string, int> countOfLandmarks = null;
       // Dictionary<string, int> countOfReasenOrders = null;
       // //public static DateTime begin ;
       // public HashSet<string> sendedToAllSign = null;
       // Dictionary<Agent, HashSet<GroundedPredicate>> agentsPublicPreconditions = null;
       // Dictionary<Agent, HashSet<GroundedPredicate>> agentsPublicEffects = null;
       // public Dictionary<string, HashSet<string>> actionMap = null;
       // public Dictionary<string, Dictionary<string, HashSet<Action>>> AgentToInfluActions = null;
       // public Dictionary<string, List<string>> influencedByAgents = null;
       // public Dictionary<string, Dictionary<string, HashSet<string>>> recoverActionEffect = null;


       // public void Preprocess(List<Agent> agents, List<Domain> lDomains, List<Problem> lProblems)
       // {
       //     List<GroundedPredicate> allGoals = new List<GroundedPredicate>();
       //     Dictionary<string, int> indexStates = new Dictionary<string, int>();

       //     // Publish all  goals
       //     foreach (Agent agent in agents)
       //     {
       //         allGoals = allGoals.Union(agent.goal).ToList();
       //         sendedToAllSign.Add(agent.name);

       //     }
       //     HashSet<GroundedPredicate> allG = new HashSet<GroundedPredicate>(allGoals);
       //     foreach (Agent agent in agents)
       //     {
       //         agent.initPlaner();
       //         PreparePublicActions(agent);
                
       //         HashSet<GroundedPredicate> preconditions = agent.GetAllPublicPreconditions();
       //         agentsPublicPreconditions.Add(agent, preconditions);
       //         agentsPublicEffects.Add(agent, agent.GetAllPublicEffects());
       //         indexStates.Add(agent.name, 0);

       //         receivedStates.Add(agent.name, new HashSet<MafsVertex>());
       //         openLists.Add(agent.name, new HashSet<MafsVertex>());
       //         mutexs.Add(agent.name, new Mutex());
       //         countOfLandmarks.Add(agent.name, agent.publicRelevantLandmark.Count);
       //         countOfReasenOrders.Add(agent.name, agent.ReasonableOrdering.Count);

       //         foreach (GroundedPredicate pGp in agent.PublicPredicates)
       //         {
       //             allPublicFacts.Add(pGp);
       //         }
       //     }
       //     MafsVertex.init(agents);
       //     List<GroundedPredicate> fullStart = new List<GroundedPredicate>();
       //     foreach (Agent a in agents)
       //     {
       //         foreach (GroundedPredicate gp in a.startState.m_lPredicates)
       //         {
       //             fullStart.Add(gp);
       //         }
       //     }
       //     HashSet<GroundedPredicate> publicStartState = new HashSet<GroundedPredicate>();
       //     Dictionary<string, State> agPriv = new Dictionary<string, State>();
       //     foreach (Agent a in agents)
       //     {
       //         State privateStartState = new State((Problem)null);

       //         foreach (GroundedPredicate gp in a.startState.m_lPredicates)
       //         {
       //             if (!a.PublicPredicates.Contains(gp))
       //                 privateStartState.AddPredicate(gp);
       //             else
       //             {
       //                 publicStartState.Add(gp);
       //             }
       //         }
       //         agPriv.Add(a.name, privateStartState);

       //     }
       //     List<MafsVertex> lVertexes = new List<MafsVertex>();
       //     Dictionary<string, MafsAgent> MapsAgents = new Dictionary<string, MafsAgent>();
       //     this.MapsAgents = new List<MafsAgent>();
       //     foreach (Agent a in agents)
       //     {
       //         MafsVertex agentStartVertex = new MafsVertex(publicStartState, agPriv[a.name], indexStates, countOfLandmarks, countOfLandmarks.Keys.ToArray(), a.name, a.allGoals.Count, countOfReasenOrders);
       //         MafsAgent mAgent = new MafsAgent(agentStartVertex, a, a.allGoals, countOfLandmarks, openLists, receivedStates, mutexs, countOfReasenOrders, fullStart);
       //         mAgent.projectionHeuristic = new AdvancedProjectionHeuristic(a, agents, lDomains, lProblems);
       //         mAgent.SetPrivateState(agPriv[a.name]);
       //         MapsAgents.Add(mAgent.name, mAgent);
       //         this.MapsAgents.Add(mAgent);
       //         lVertexes.Add(agentStartVertex);
       //     }
       //     MafsVertex.UpDateAgents(MapsAgents);
       //     foreach (MafsVertex v in lVertexes)
       //     {
       //         v.PreaperFirstVertex();
       //     }
       //     foreach (Agent a in agents.ToList())
       //     {
       //         recoverActionEffect.Add(a.name, new Dictionary<string, HashSet<string>>());
       //         foreach (Action act in a.publicActions)
       //         {
       //             recoverActionEffect[a.name].Add(act.Name, new HashSet<string>());
       //             bool breaking = false;
       //             foreach (GroundedPredicate eff in act.HashEffects)
       //             {
       //                 if (eff.Negation)
       //                 {
       //                     foreach (Agent a2 in agents.ToList())
       //                     {
       //                         if (a2.name.Equals(a.name))
       //                             continue;
       //                         HashSet<GroundedPredicate> agentEff = agentsPublicEffects[a2];
       //                         if (agentEff.Contains((GroundedPredicate)eff.Negate()))
       //                         {
       //                             recoverActionEffect[a.name][act.Name].Add(a2.name);
       //                             breaking = true;
       //                             break;
       //                         }
       //                     }
       //                     if (breaking)
       //                         break;
       //                 }
       //             }

       //         }
       //         AgentToInfluActions.Add(a.name, new Dictionary<string, HashSet<Action>>());
       //         foreach (Action act in a.publicActions)
       //         {
       //             if (!actionMap.ContainsKey(act.Name))
       //                 actionMap.Add(act.Name, new HashSet<string>());
       //             foreach (var pair in agentsPublicPreconditions)
       //             {
       //                 //  if (a.Equals(pair.Key))
       //                 //    continue;
       //                 bool flag = false;
       //                 foreach (GroundedPredicate eff in act.HashEffects)
       //                 {
       //                     if (pair.Value.Contains(eff))
       //                     {
       //                         flag = true;
       //                         break;
       //                     }
       //                 }
       //                 if (flag)
       //                 {
       //                     actionMap[act.Name].Add(pair.Key.name);
       //                 }
       //             }
       //         }
       //         influencedByAgents.Add(a.name, new List<string>());
       //         foreach (Agent a2 in agents.ToList())
       //         {
       //             AgentToInfluActions[a.name].Add(a2.name, new HashSet<Action>());
       //             foreach (Action act in a.publicActions)
       //             {

       //                 bool effFlag = false;
       //                 foreach (GroundedPredicate eff in act.HashEffects)
       //                 {
       //                     if (agentsPublicPreconditions[a2].Contains(eff))
       //                     {
       //                         effFlag = true;
       //                         break;
       //                     }
       //                 }

       //                 if (effFlag)
       //                 {
       //                     AgentToInfluActions[a.name][a2.name].Add(act);
       //                 }
       //             }


       //             if (a.name.Equals(a2.name))
       //                 continue;
       //             bool flag = false;
       //             foreach (GroundedPredicate pre in agentsPublicPreconditions[a])
       //             {
       //                 if (agentsPublicEffects[a2].Contains(pre))
       //                 {
       //                     influencedByAgents[a.name].Add(a2.name);
       //                     break;
       //                 }
       //             }
       //         }
       //     }           

       // }

       // /// <summary>
       // /// Prepare data structures relevant for the public actions
       // /// </summary>
       // /// <param name="agent"></param>
       // private void PreparePublicActions(Agent agent)
       // {
       //     foreach (Action publicAction in agent.publicActions)
       //     {
       //         // Mark the actions that have an effect that deletes a public predicate
       //         foreach (GroundedPredicate effect in publicAction.HashEffects)
       //         {
       //             if ((agent.PublicPredicates.Contains(effect) ||
       //                 agent.PublicPredicates.Contains((GroundedPredicate)effect.Negate()))
       //                 && effect.Negation)
       //             {
       //                 publicAction.isDeletePublic= true;
       //                 break;
       //             }
       //         }


       //         // Set prevail preconditions as effects (<todo> ask shlomi why </todo> )
       //         foreach (GroundedPredicate precondition in publicAction.HashPrecondition)
       //         {
       //             if (agent.PublicPredicates.Contains(precondition))
       //             {
       //                 if (publicAction.HashEffects.Contains((GroundedPredicate)precondition.Negate()))
       //                     continue;
       //                 ((CompoundFormula)publicAction.Effects).AddOperand(precondition);
       //                 publicAction.HashEffects.Add(precondition);
       //             }

       //         }
       //     }
       // }

        
        
       // public SimpleMafsPlanner()
       // {
       //     agentsPublicPreconditions = new Dictionary<Agent, HashSet<GroundedPredicate>>();
       //     agentsPublicEffects = new Dictionary<Agent, HashSet<GroundedPredicate>>();
       //     recoverActionEffect = new Dictionary<string, Dictionary<string, HashSet<string>>>();
       //     List<GroundedPredicate> allGoals = new List<GroundedPredicate>();
       //     sendedToAllSign = new HashSet<string>();
       //     actionMap = new Dictionary<string, HashSet<string>>();
       //     AgentToInfluActions = new Dictionary<string, Dictionary<string, HashSet<Action>>>();
       //     influencedByAgents = new Dictionary<string, List<string>>();
       //     stop = false;
       //     MacroAction.counter = 1;
       //     receivedStates = new Dictionary<string, HashSet<MafsVertex>>();
       //     openLists = new Dictionary<string, HashSet<MafsVertex>>();
       //     mutexs = new Dictionary<string, Mutex>();
       //     countOfLandmarks = new Dictionary<string, int>();
       //     countOfReasenOrders = new Dictionary<string, int>();
       //     allPublicFacts = new HashSet<GroundedPredicate>();

            
       // }
        
        
        
       // public List<string> Plan()
       // {
       //     //List<Thread> threads = new List<Thread>();
       //     //begin = DateTime.Now;
       //     Program.countMacro = 0.0;
       //     Program.ffMessageCounter = 0;
       //     Program.countAvgPerMacro = 0.0;
       //     Program.sendedStateCounter = 0;
       //     Program.StateExpendCounter = 0;
       //     ImprovedMafsPlanner.generateCounter = 0;
       //     Program.notSandedStates = 0;
       //     List<string> lplan = null;
       //     while (lplan == null)
       //     {
       //         foreach (MafsAgent agent in MapsAgents)
       //         {
       //             lplan = agent.BeginPlanning();
       //             if (lplan != null)
       //                 break;

       //         }


       //         if (lplan != null)
       //         {
       //             ImprovedMafsPlanner.stop = true;
       //             break;
       //         }

       //     }
       //     return lplan;

       // }
        
        
       // public static bool findGoal=false;
       // public static List<string> finalPlan = null;
       // //public static Mutex goalChackMutex = null;
        
       //// public static Dictionary<string, Mutex> globalListMutex = null;
       // public List<string> DistrebutedPreferablePlan()
       // {
       //     //List<Thread> threads = new List<Thread>();
       //     //begin = DateTime.Now;
       //     // goalChackMutex = new Mutex();
       //     // heursticCalcultionMutex = new Mutex();
       //     //tmpMutex = new Mutex();

       //     MafsAgent.InitMutex(MapsAgents);
       //     Program.countMacro = 0.0;
       //     Program.ffMessageCounter = 0;
       //     Program.countAvgPerMacro = 0.0;
       //     Program.sendedStateCounter = 0;
       //     Program.StateExpendCounter = 0;
       //     Program.notSandedStates = 0;
       //     ImprovedMafsPlanner.generateCounter = 0;
       //     List<string> lplan = null;
       //     List<Thread> threads = new List<Thread>();
       //     /*
       //     for(int i=MapsAgents.Count-1;i>-1;i--)
       //     {
       //         MapsAgent agent = MapsAgents.ElementAt(i);
       //         Thread t = new Thread(agent.BeginDistrebutedPreferablePlanning);
       //         threads.Add(t);
       //         t.Start();
       //     }
       //     */
       //    foreach (MafsAgent agent in MapsAgents)
       //     {
       //         Thread t = new Thread(agent.BeginDistrebutedPreferablePlanning);
       //         t.Name = agent.name;
       //         threads.Add(t);                
       //         t.Start();              
       //     }

       //   foreach(Thread t in threads)
       //     {
       //         t.Join();
       //     }

            
       //     return finalPlan;

       // }
    }
}
