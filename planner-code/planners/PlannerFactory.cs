﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planning.planners
{
    class PlannerFactory
    {
        static PlannerFactory instance = new PlannerFactory();

        public static PlannerFactory GetInstance(){
            return instance;
        }

        public AbstractPlanner CreateSimpleMafsPlanner(ProblemSolver solver)
        {
            return new SimpleMafsPlanner(solver);
        }
    }
}
