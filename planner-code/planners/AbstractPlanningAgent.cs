﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.CompilerServices;

namespace Planning.planners
{
    abstract class AbstractPlanningAgent
    {
        private static int agentIdSequence=0; // A sequence for getting a unique agent ID for each agent

        public int agentId; // A unique identifier of the agent

        // Agent's view
        protected Problem problem;
        protected Domain domain;
        protected List<AbstractPlanningAgent> agentTeam;


        // ---- Convenience datastructure ---
        protected GlobalState initialState;
        protected GlobalState goalState;


        // Message passing
        protected List<Message> inbox;

        public AbstractPlanningAgent(Problem problem, Domain domain, List<AbstractPlanningAgent> agentTeam)
        {
            this.problem = problem;
            this.domain = domain;
            this.agentTeam = agentTeam;
            this.agentId = GetNextAgentId();
        }

        /// <summary>
        /// Generate a unique agent Id
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static int GetNextAgentId()
        {
            agentIdSequence++;
            return agentIdSequence;
        }

        /// <summary>
        /// Checks if the given agent has the given action
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public bool HasAction(Action action)
        {
            return this.domain.Actions.Contains(action);
        }

        /// <summary>
        /// Set the start and goal states and prepare to start planning
        /// </summary>
        /// <param name="initialState"></param>
        /// <param name="goalState"></param>
        public virtual void Setup(GlobalState initialState, GlobalState goalState)
        {
            // Agent's view of the domain
            this.initialState = initialState;
            this.goalState = goalState;

            this.inbox = new List<Message>();
        }
        
        /// <summary>
        /// Perform a single planning iteration. 
        /// <todo> Better explain the concept of iteration </todo>
        /// </summary>
        /// <returns>True, iff a goal is found</returns>
        public abstract bool SinglePlanningIteration();



        /// <summary>
        /// Return the list of available actions in the given global state
        /// </summary>
        /// <param name="state">The global state</param>
        /// <returns>A list of actions that can be performed</returns>
        public List<Action> GetPossibleActions(GlobalState state)
        {
            HashSet<Predicate> agentView = state.GetAgentView(this);
            List<Action> possibleActions = new List<Action>();
            foreach (Action action in this.domain.PublicActions) // TODO: BUG!!! this.domain.actions
                if (CanDo(action,agentView))
                    possibleActions.Add(action);

            return possibleActions;
        }

        /// <summary>
        /// Checks if the given action can be performed, given an agent's view of the true predicates in the world
        /// </summary>
        /// <param name="action"></param>
        /// <param name="agentView"></param>
        /// <returns>True if action can be performed, false otherwise</returns>
        private bool CanDo(Action action, HashSet<Predicate> agentView)
        {
            return action.Preconditions.IsTrue(agentView,true);
        }


        /// <summary>
        /// Apply an action performed by this agent to the current state
        /// and return the resulting global state.
        /// </summary>
        /// <param name="action">One of the agent's actions</param>
        /// <param name="currentState">The current global state</param>
        /// <returns>A new global state, that is the results of applying the given action by this agent</returns>
        public GlobalState Apply(Action action, GlobalState currentState)
        {
            PrivateState privateState = currentState.agentToPrivateState[this];
            HashSet<Predicate> newPublicPredicates = new HashSet<Predicate>(currentState.publicPredicates);
            HashSet<Predicate> newPrivatePredicates = new HashSet<Predicate>(privateState.privatePredicates);

            Formula effects = action.Effects;
            if (effects is CompoundFormula)
            {
                foreach (Predicate effect in ((CompoundFormula)effects).GetAllPredicates())
                {
                    if (this.domain.IsPublic(effect))
                        ApplyEffect(effect, newPublicPredicates);
                    else
                        ApplyEffect(effect, newPrivatePredicates);
                }
            }
            else if (effects is PredicateFormula)
            {
                Predicate effect = ((PredicateFormula)effects).Predicate;
                if (this.domain.IsPublic(effect))
                    ApplyEffect(effect, newPublicPredicates);
                else
                    ApplyEffect(effect, newPrivatePredicates);
            }
            else throw new NotImplementedException(); // Support for conditional effects is left for future work

            PrivateState newPrivateState = new PrivateState(newPrivatePredicates);

            // Replicate pointers to private states of all agents
            Dictionary<AbstractPlanningAgent,PrivateState> newAgentToPrivateState = new Dictionary<AbstractPlanningAgent,PrivateState>(currentState.agentToPrivateState);
            newAgentToPrivateState[this]=newPrivateState;

            return new GlobalState(newPublicPredicates, newAgentToPrivateState);
        }



        /// <summary>
        /// Populate the add-list/delete-list with the predicates that should be added/deleted according to the forumal effect.
        /// <todo>This is mainly here to support future implementation of more sophisticated forms of effects</todo>
        /// </summary>
        /// <param name="action">The action for which to get the effects</param>
        /// <param name="add">This list will be populated by the add effects</param>
        /// <param name="delete">This list will be populated by the delete effects</param>
        private void PopulateEffectsLists(Action action, HashSet<Predicate> add, HashSet<Predicate> delete)
        {
            Formula effects = action.Effects;
            if (effects is CompoundFormula)
            {
                foreach (Predicate effect in ((CompoundFormula)effects).GetAllPredicates())
                {
                    if(effect.Negation)
                        delete.Add(effect);
                    else
                        add.Add(effect);
                }
            }
            else if (effects is PredicateFormula)
            {
                Predicate effect = ((PredicateFormula)effects).Predicate;
                if (effect.Negation)
                    delete.Add(effect);
                else
                    add.Add(effect);
            }
            else // Support for conditional effects is left for future work
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Apply the effect on the state predicates. 
        /// This means removing negeated state predicates
        /// and adding new state predicate
        /// </summary>
        /// <param name="effect"></param>
        /// <param name="statePredicates"></param>
        private void ApplyEffect(Predicate effect, HashSet<Predicate> statePredicates)
        {
            if (!(effect is GroundedFunctionPredicate))
            {
                if (effect.Negation)
                {
                    Predicate pNegateEffect = effect.Negate();
                    if (statePredicates.Contains(pNegateEffect))
                        statePredicates.Remove(pNegateEffect);
                }
                else // Effect is a positive -- add it
                {
                    statePredicates.Add(effect);
                }
                /* <todo> this may return if Shlomi is convincing </todo>
                if (!statePredicates.Contains(effect))
                {
                    statePredicates.Add(effect);//we are maintaining complete state information
                }
                 * */
            }
        }





        /// <summary>
        /// Send a message to a fellow agent
        /// </summary>
        /// <param name="content"></param>
        /// <param name="toAgent"></param>
        public void Send(Object content, AbstractPlanningAgent toAgent)
        {
            toAgent.Recieve(content, this);
        }

        /// <summary>
        /// Recieve a message from a fellow agent
        /// </summary>
        /// <param name="content"></param>
        /// <param name="fromAgent"></param>
        public void Recieve(Object content, AbstractPlanningAgent fromAgent)
        {
            this.inbox.Add(new Message(content,fromAgent)); 
        }

        /// <summary>
        /// Objects of this class represents a message sent by one of the agents
        /// </summary>
        public class Message
        {
            public Object content;
            public AbstractPlanningAgent sender;

            public Message(Object content, AbstractPlanningAgent sender)
            { this.content = content; this.sender = sender; }
        }
    }
}
