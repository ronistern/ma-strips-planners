﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planning.planners
{
    class GlobalState
    {
        private int hashCode=-1; // The hashcode of this object. It is stored to make computations efficient

        public HashSet<Predicate> publicPredicates; // The public predicates that are true in the state
        public Dictionary<AbstractPlanningAgent, PrivateState> agentToPrivateState; // Maps each agent to the private state, containing the private predicats in this global state

        public GlobalState(HashSet<Predicate> publicPredicates,
            Dictionary<AbstractPlanningAgent, PrivateState> agentToPrivateState)
        {
            this.publicPredicates = publicPredicates;
            this.agentToPrivateState = agentToPrivateState;
        }


        /// <summary>
        /// Return the given agent's view of this state. 
        /// This means the public predicates, and the agent's private predicates
        /// </summary>
        /// <param name="agent"></param>
        /// <returns></returns>
        public HashSet<Predicate> GetAgentView(AbstractPlanningAgent agent)
        {
            HashSet<Predicate> agentView = new HashSet<Predicate>();
            agentView.UnionWith(publicPredicates);
            agentView.UnionWith(agentToPrivateState[agent].privatePredicates);
            return agentView;
        }


        /// <summary>
        /// Override equals to compare content and not pointers
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            // Compare the global predicates
            if (this.publicPredicates.SetEquals(((GlobalState)obj).publicPredicates) == false)
                return false;
            
            // Compare private states dictionary
            Dictionary<AbstractPlanningAgent,PrivateState> otherAgentToPrivateState = ((GlobalState)obj).agentToPrivateState;           
            return (agentToPrivateState.Intersect(otherAgentToPrivateState).Count()==
                    agentToPrivateState.Union(otherAgentToPrivateState).Count());
        }

        /// <summary>
        /// Override hash code to have the hash based on content and not pointers
        /// </summary>
        /// <returns>The hashcode for this object</returns>
        public override int GetHashCode()
        {
            if (hashCode != -1) return hashCode;
            
            foreach (var kv in this.agentToPrivateState)
            {
                hashCode += kv.Key.GetHashCode() + kv.Value.GetHashCode();
            }
            foreach (Predicate publicPredicate in this.publicPredicates)
            {
                hashCode += publicPredicate.GetHashCode();
            }
            return hashCode;
        }

        /// <summary>
        /// Checks if this state is true in the other state.
        /// Checks subsumption of predicates (assuming predicates are only positives)
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool IsTrue(GlobalState other)
        {
            if (this.publicPredicates.IsSubsetOf(other.publicPredicates) == false)
                return false;
            foreach (AbstractPlanningAgent agent in this.agentToPrivateState.Keys)
                if (this.agentToPrivateState[agent].privatePredicates.IsSubsetOf(other.agentToPrivateState[agent].privatePredicates) == false)
                    return false;
            return true;
        }
        

        /// <summary>
        ///  Used for outputting the content of the state for logging purposes
        /// </summary>
        /// <returns></returns>
        public string ToLog()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Public:\n");
            AppendPredicateList(builder, "\t", "\n", this.publicPredicates);

            foreach (AbstractPlanningAgent agent in this.agentToPrivateState.Keys)
            {
                builder.Append("Private ");
                builder.Append(agent.agentId);
                builder.Append(":\n");
                AppendPredicateList(builder,"\t","\n", this.agentToPrivateState[agent].privatePredicates);
            }
            return builder.ToString();
        }

        /// <summary>
        /// Convenience function to print a list of predicates
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="prefix"></param>
        /// <param name="suffix"></param>
        /// <param name="predicates"></param>
        private void AppendPredicateList(StringBuilder builder, string prefix, string suffix,IEnumerable<Predicate> predicates)
        {
            foreach (Predicate predicate in predicates)
            {
                builder.Append(prefix);
                builder.Append(predicate);
                builder.Append(suffix);
            }
        }
    }
}
