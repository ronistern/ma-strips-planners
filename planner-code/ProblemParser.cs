﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Planning
{
    class ProblemParser
    {
        /// <summary>
        /// Create an MA-STRIPS problem from the domain and problem files in the given directory. 
        /// The given directory should contain a domain and problem file for every agent, representing each agent's view
        /// of the domain and the problem at hand (the start and goal state). 
        /// </summary>
        /// <param name="inputProblemsDir">The directory where the domain and problem files are</param>
        /// <returns></returns>
        public MaStripsProblem ParseProblem(DirectoryInfo inputProblemsDir)
        {
            // Identifying the domain and problem files
            Console.WriteLine("Reading files " + inputProblemsDir.Name);
            List<string> domainFiles = new List<string>();
            List<string> problemFiles = new List<string>();
            foreach (FileInfo fileInfo in inputProblemsDir.GetFiles())
            {
                if (fileInfo.Name.Contains("domain"))
                    domainFiles.Add(fileInfo.FullName);
                if (fileInfo.Name.Contains("problem"))
                    problemFiles.Add(fileInfo.FullName);
            }

            
            // Parsing the domain and problem files
            Console.WriteLine("Parsing");
            Domain newDomain;
            Problem newProblem;
            Parser parser = new Parser();
            
            Domain.ResetStaticVar();
            List<Domain> domains = new List<Domain>();
            List<Problem> problems = new List<Problem>();

            for (int i = 0; i < domainFiles.Count; i++)
            {
                newDomain = parser.ParseDomain(domainFiles[i]);
                newProblem = parser.ParseProblem(problemFiles[i], newDomain);

                domains.Add(newDomain);
                problems.Add(newProblem);
            }
            RemoveFalseConstants(domains);  // This is probably redundant: removes predicates that are declared in the PDDL as constants but are in action effects      


            // Initialize the private and public actions of each domain
            foreach (Domain domain in domains)
                domain.InitializePublicAndPrivateActions();

            return new MaStripsProblem(domains, problems,inputProblemsDir);
        }



        /// <summary>
        /// Remove all predicates that were declared constants but appear in the effect of some public action. 
        /// <todo> Check if this has any effect </todo>
        /// </summary>
        /// <param name="domains"></param>
        private void RemoveFalseConstants(List<Domain> domains)
        {
            foreach (Domain domain in domains)
            {
                foreach (Action action in domain.PublicActions)
                {
                    foreach (Predicate predicate in action.Effects.GetAllPredicates())
                    {
                        foreach (Domain dOther in domains)
                        {
                            if (dOther.AlwaysConstant(predicate))
                                dOther.m_lAlwaysConstant.Remove(predicate.Name);

                        }
                    }

                }

            }
        }



        /// <summary>
        /// Generate a single agent domain from a domain the was defined for multiple agents.
        /// This means:
        /// 1. The set of actions is the union of all agents' actions.
        /// 2. The set of predicates is the union of all agents' predicates and constants.
        /// 3. The goal is the conjunction (AND) of all agents' goals. 
        /// </summary>
        /// <param name="lDomains"></param>
        /// <param name="lProblems"></param>
        /// <param name="dJoint"></param>
        /// <param name="pJoint"></param>
        public void GetJointDomain(List<Domain> lDomains, List<Problem> lProblems, out Domain dJoint, out Problem pJoint)
        {
            dJoint = lDomains[0].Clone();
            pJoint = lProblems[0].Clone();
            pJoint.Domain = dJoint;
            CompoundFormula cfAnd = new CompoundFormula("and");
            cfAnd.AddOperand(pJoint.Goal);
            for (int i = 1; i < lDomains.Count; i++)
            {
                dJoint.Actions.AddRange(lDomains[i].Actions);
                dJoint.Predicates.UnionWith(lDomains[i].Predicates);
                dJoint.Constants.UnionWith(lDomains[i].Constants);
                foreach (Predicate pKnown in lProblems[i].Known)
                    pJoint.AddKnown(pKnown);
                cfAnd.AddOperand(lProblems[i].Goal);
            }
            pJoint.Goal = cfAnd.Simplify();
        }

        /// <summary>
        /// Setup the PDB paths. This is needed for the PDB heuristic function.
        /// </summary>
        /// <param name="inputProblemsDir">The directory where the problem is described (where the domain and problem files are)</param>
        /// <returns>A path to the directory where to create the PDB</returns>
        public string SetupPdbDirectory(DirectoryInfo inputProblemsDir)
        {
            string pdbPath = @"PdbFiles/" + inputProblemsDir.Parent.Name;
            if (!Directory.Exists(pdbPath))
            {
                Directory.CreateDirectory(pdbPath);
            }
            pdbPath += "/" + inputProblemsDir.Name + ".pdb";
            return pdbPath;
        }

    }
}
