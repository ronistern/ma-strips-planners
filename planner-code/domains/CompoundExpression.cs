﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Planning
{
    class CompoundExpression : Expression
    {
        public List<Expression> subExpressions { get; private set; }
        public string Type
        {
            get;
            set;
        }
        public CompoundExpression()
        {
            subExpressions = new List<Expression>();
        }
        public override string ToString()
        {
            string s = "(" + Type;
            foreach (Expression e in subExpressions)
            {
                s += " " + e.ToString();
            }
            s += ")";
            return s;
        }
    }
}
