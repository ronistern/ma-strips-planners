﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Planning
{
    /// <summary>
    /// An instance of this class represents an MA-STRIPS planning problem.
    /// </summary>
    class MaStripsProblem
    {
        public List<Domain> domains;
        public List<Problem> problems;
        public DirectoryInfo inputDirectory; // Not the best design to have this here or to have this public

        private Domain jointDomain;        
        private Problem jointProblem;        
        private string pdbDir; // Not the best design to have this here


        public MaStripsProblem(List<Domain> domains, List<Problem> problems, DirectoryInfo inputDirectory)
        {
            this.domains=domains;
            this.problems=problems;
            this.inputDirectory = inputDirectory;
            jointDomain=null;
            jointProblem=null;
            pdbDir = null;
        }

        /// <summary>
        /// Return the domain of this MA-STRIPS problem as if it was a single agent one. 
        /// </summary>
        /// <returns></returns>
        public Domain GetJointDomain()
        {
            if (this.jointDomain==null)
            {
                ProblemParser parser = new ProblemParser();
                parser.GetJointDomain(this.domains, this.problems, out this.jointDomain, out this.jointProblem);
            }
            return this.jointDomain;
        }


        /// <summary>
        /// Return this MA-STRIPS problem as if it was a single agent problem. 
        /// </summary>
        /// <returns></returns>
        public Problem GetJointProblem()
        {
            if (this.jointProblem == null)
            {
                ProblemParser parser = new ProblemParser();
                parser.GetJointDomain(this.domains, this.problems, out this.jointDomain, out this.jointProblem);
            }
            return this.jointProblem; 
        }


        /// <summary>
        /// The directory where to read/write the PDB.
        /// Used when using the PDB heuristic
        /// </summary>
        /// <returns></returns>
        public string GetPdbDirectory()
        {
            if (this.pdbDir == null)
            {
                ProblemParser parser = new ProblemParser();
                this.pdbDir=parser.SetupPdbDirectory(this.inputDirectory);
            }
            return this.pdbDir;
        }
    }
}
