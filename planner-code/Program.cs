﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Threading;


namespace Planning
{
    class Program
    {
        // Timeout for trying to solve a single planning problem
        private const int TIMEOUT_PER_INSTANCE = 30 * 60 * 1000;



        // public static StreamWriter timeResults = new StreamWriter("timeResults.txt", false);
        static public int notSandedStates = 0;
        public static int sendedStateCounter = 0;
        public static string resultFilePath = "plan.txt";
        public static int StateExpendCounter = 0;
        public static double countMacro = 0.0;
        public static double countAvgPerMacro = 0.0;
        static string pdbPath;
        public enum PlanerType { ff_tryCoordinate, hsp_tryCoordinate, ff_directPlan, hsp_directPlan, ff_toActions };
        public enum HighLevelPlanerType { PDB, Landmark, Projection, ForwardHsp, BackwardHsp, LandmarkAndHsp, WeightedLandmarkAndHsp, SophisticatedProjection, MafsLandmark, Mafsff, MafsWithProjectionLandmarks, PDBMafs, ProjectionMafs, DistrebutedProjectionMafs };
        static public HighLevelPlanerType highLevelPlanerType = HighLevelPlanerType.LandmarkAndHsp;
        static public PlanerType internalPlaner;
        public enum ProjectionVersion { Local, Global, GlobalV2, GlobalWithMemo, fullGlobal,ProjectionFF,NULL };
        static public ProjectionVersion projectionVersion = ProjectionVersion.ProjectionFF;
        static public List<double> times = new List<double>();
        static public List<double> countActions = new List<double>();
        static public double timeSum = 0;
        static public double actionSum = 0;
        static public double messages = 0;
        static public double messages2 = 0;
        static public double messages3 = 0;
        static public int countOfDisLandmarks = 0;
        static public bool complexLandmarks = true;
        static public PatternDatabase pdb = null;

        public static DateTime Start, StartHighLevelPlanning, StartGrounding, End;
        public static int PlanCost, PlanMakeSpan;

        public static int countOfProjAction = 0;
        public static int countOfProjFact = 0;
        public static int sizeOfRegressionTree = 0;
        public static int maxSizeOfRegressionTree = 0;
        public static int maxDepthOfRegressionTree = 0;


        

        public static bool VerifyPlan(Domain dJoint, Problem pJoint, List<string> lPlan)
        {
            List<State> lStates = new List<State>();
            State sInitial = new State(pJoint);
            foreach (Predicate p in pJoint.Known)
                sInitial.AddPredicate(p);
            State sCurrent = sInitial;
            int i = 0;
            foreach (string sAction in lPlan)
            {
                State sNew = sCurrent.Apply(sAction);
                if (sNew == null)
                    return false;
                lStates.Add(sNew);
                sCurrent = sNew;
                i++;
            }
            CompoundFormula joinGoal = new CompoundFormula("and");
            foreach (GroundedPredicate gGp in pJoint.Goal.GetAllPredicates())
            {
                if (!gGp.Name.Equals("and"))
                    joinGoal.AddOperand(gGp);
            }
            pJoint.Goal = joinGoal;
            if (pJoint.IsGoalState(sCurrent))
            {
                PlanCost = lPlan.Count;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Generate a single agent domain from a domain the was defined for multiple agents.
        /// This means:
        /// 1. The set of actions is the union of all agents' actions.
        /// 2. The set of predicates is the union of all agents' predicates and constants.
        /// 3. The goal is the conjunction (AND) of all agents' goals. 
        /// </summary>
        /// <param name="lDomains"></param>
        /// <param name="lProblems"></param>
        /// <param name="dJoint"></param>
        /// <param name="pJoint"></param>
        public static void GetJointDomain(List<Domain> lDomains, List<Problem> lProblems, out Domain dJoint, out Problem pJoint)
        {
            dJoint = lDomains[0].Clone();
            pJoint = lProblems[0].Clone();
            pJoint.Domain = dJoint;
            CompoundFormula cfAnd = new CompoundFormula("and");
            cfAnd.AddOperand(pJoint.Goal);
            for (int i = 1; i < lDomains.Count; i++)
            {
                dJoint.Actions.AddRange(lDomains[i].Actions);
                dJoint.Predicates.UnionWith(lDomains[i].Predicates);
                dJoint.Constants.UnionWith(lDomains[i].Constants);
                foreach (Predicate pKnown in lProblems[i].Known)
                    pJoint.AddKnown(pKnown);
                cfAnd.AddOperand(lProblems[i].Goal);
            }
            pJoint.Goal = cfAnd.Simplify();
        }

        /// <summary>
        /// Reads the current directory and solve the planning problem in it.
        /// </summary>
        /// <param name="inputProblemsDir"></param>
        /// <param name="outputPlanFile"></param>
        static void RunExperiment(DirectoryInfo inputProblemsDir, string outputPlanFile)
        {
            ProblemParser parser = new ProblemParser();
            MaStripsProblem maStripsProblem = parser.ParseProblem(inputProblemsDir);
            List<Domain> lDomains = maStripsProblem.domains;
            List<Problem> lProblems = maStripsProblem.problems;
            Domain dJoint = maStripsProblem.GetJointDomain();
            Problem pJoint = maStripsProblem.GetJointProblem();

            // Required setup when using the PDB heursitic (basically sets the directory where the PDBs should be in
            SetupPDBs(inputProblemsDir);

            ProblemSolver solver = new ProblemSolver();
            List<string> lPlan = solver.SolveFactored(maStripsProblem);
                //SolveFactored(lDomains, lProblems, ref agents, dJoint);

            // Case 1: No plan has returned (e.g., there is no solution to the problem)
            if (lPlan == null)
            {
                WritePlanToFile(new List<string>(), outputPlanFile);
                WriteResults(inputProblemsDir.Name, " fail, plan is null");
                return;
            }

            // Case 2: A plan has been found, but it is not valid (this indicates that a bug has occured in the planner)
            if (VerifyPlan(dJoint, pJoint, lPlan) == false)
            {
                WriteResults(inputProblemsDir.Name, " plan verification failed");
                throw new System.InvalidOperationException("Generated an invalid plan");
            }

            // Case 3: A valid (sound) plan has been found!
            WritePlanToFile(lPlan, outputPlanFile);
            WriteResults(inputProblemsDir.Name, " success");
            Console.WriteLine();
        }



        /// <summary>
        /// Setup the PDB paths. This is needed for the PDB heuristic function.
        /// </summary>
        /// <param name="inputProblemsDir">The directory where the problem is described (where the domain and problem files are)</param>
        private static void SetupPDBs(DirectoryInfo inputProblemsDir)
        {
            pdbPath = @"PdbFiles/" + inputProblemsDir.Parent.Name;
            if (!Directory.Exists(pdbPath))
            {
                Directory.CreateDirectory(pdbPath);
            }
            pdbPath += "/" + inputProblemsDir.Name + ".pdb";
        }
        public static List<KeyValuePair<string, Action>> GetActions(List<string> lActionsName, Domain dJoint, List<Agent> agents)
        {
            List<KeyValuePair<string, Action>> sPlan = new List<KeyValuePair<string, Action>>();
            foreach (string actName in lActionsName)
            {
                string sOutputName = Domain.MapGroundedActionNamesToOutputNames[actName];
                sOutputName = sOutputName.Substring(1, sOutputName.Length - 2);
                Action act = dJoint.GroundActionByName(sOutputName.Split(' '));
                bool fails = true;
                foreach (Agent agent in agents)
                {
                    if (agent.m_actions.Contains(act))
                    {
                        sPlan.Add(new KeyValuePair<string, Action>(agent.name, act));
                        fails = false;
                        break;
                    }
                }
                if (fails)
                    throw new Exception();
            }
            return sPlan;
        }
        
        public static int MaxCost(Dictionary<GroundedPredicate, int> cost, List<Predicate> pre)
        {
            int MaxCost = 0;
            foreach (GroundedPredicate preGp in pre)
            {
                if (cost[preGp] > MaxCost)
                {
                    MaxCost = cost[preGp];
                }
            }
            return MaxCost;
        }


        static void ReadAgentFiles(List<string> lDomainFiles, List<string> lProblemFiles, List<Domain> lDomains, List<Problem> lProblems)
        {
            Console.WriteLine("Parsing");
            for (int i = 0; i < lDomainFiles.Count; i++)
            {
                Parser parser = new Parser();
                Domain d = parser.ParseDomain(lDomainFiles[i]);
                Problem p = parser.ParseProblem(lProblemFiles[i], d);

                lDomains.Add(d);
                lProblems.Add(p);
            }
            foreach (Domain d in lDomains)
            {
                foreach (Action a in d.PublicActions)
                {
                    foreach (Predicate p in a.Effects.GetAllPredicates())
                    {
                        foreach (Domain dOther in lDomains)
                        {
                            if (dOther.AlwaysConstant(p))
                                dOther.m_lAlwaysConstant.Remove(p.Name);

                        }
                    }

                }

            }
        }

        public static double pdbCreationTime;


        static List<string> SolveFactored(List<Domain> lDomains, List<Problem> lProblems, ref List<Agent> m_agents, Domain joinDomain)
        {
            if( highLevelPlanerType != HighLevelPlanerType.ProjectionMafs)
            {
                projectionVersion = ProjectionVersion.NULL;
            }
            Start = DateTime.Now;
            pdbCreationTime = 0;
            Console.WriteLine("Grounding");
            BuildAgents.mapActionNameToAgents = new Dictionary<string, HashSet<string>>();
            List<Domain> lGrounded = GroundDomains(lDomains, lProblems);


            internalPlaner = PlanerType.ff_toActions;
            BuildAgents buildAgents = new BuildAgents(lDomains, lGrounded, lProblems);
            List<Agent> agents = buildAgents.ReturnAgents();

            Dictionary<string, HashSet<GroundedPredicate>> allPublicPredicate = new Dictionary<string, HashSet<GroundedPredicate>>();
            foreach (Agent agent in agents)
            {

                allPublicPredicate.Add(agent.name, new HashSet<GroundedPredicate>());
                foreach (Action publicAct in agent.publicActions)
                {
                    foreach (GroundedPredicate pre in publicAct.HashPrecondition)
                    {
                        if (agent.PublicPredicates.Contains(pre))
                        {
                            allPublicPredicate[agent.name].Add(pre);
                        }
                    }

                    foreach (GroundedPredicate eff in publicAct.HashEffects)
                    {
                        if (agent.PublicPredicates.Contains(eff))
                        {
                            allPublicPredicate[agent.name].Add(eff);
                        }
                    }
                }
                /* foreach (GroundedPredicate gp in agent.PublicPredicates)
                 {
                     allPublicPredicate[agent.name].Add(gp);
                 }*/
            }
            foreach (Agent agent in agents)
            {
                // agent.ReducePublicActions(allPublicPredicate);
            }
            List<string> lPlan = null;

            if (highLevelPlanerType == HighLevelPlanerType.Projection)
            {
                Console.WriteLine("Planning");
                bool stop = false;
                while (!stop)
                {
                    stop = true;
                    string name = "";
                    GroundedPredicate currentGoal = null;
                    foreach (Agent agent in agents)
                    {
                        currentGoal = agent.GetGoal();
                        if (currentGoal != null)
                        {
                            stop = false;
                            name = agent.name;
                            break;
                        }
                    }
                    if (!stop)
                    {
                        foreach (Agent agent in agents)
                        {
                            if (!agent.name.Equals(name))
                            {
                                agent.ReceiveGoal(currentGoal);
                            }
                        }
                    }

                }
                AdvancedLandmarkProjectionPlaner planner = new AdvancedLandmarkProjectionPlaner();
                lPlan = planner.Plan(agents, lDomains, lProblems, joinDomain);
            }
            else
            {
                try
                {

                    StartHighLevelPlanning = DateTime.Now;
                    if (highLevelPlanerType == HighLevelPlanerType.Landmark)
                    {
                        Console.WriteLine("Identifying landmarks");
                        bool stop = false;
                        while (!stop)
                        {
                            stop = true;
                            string name = "";
                            GroundedPredicate currentGoal = null;
                            foreach (Agent agent in agents)
                            {
                                currentGoal = agent.GetGoal();
                                if (currentGoal != null)
                                {
                                    stop = false;
                                    name = agent.name;
                                    break;
                                }
                            }
                            if (!stop)
                            {
                                foreach (Agent agent in agents)
                                {
                                    if (!agent.name.Equals(name))
                                    {
                                        agent.ReceiveGoal(currentGoal);
                                    }
                                }
                            }

                        }
                        foreach (Agent agent in agents)
                        {
                            agent.InitMutex();
                        }

                        agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);
                        Planer_I Planner = new Planer_I(agents);
                        Console.WriteLine("Planning");
                        lPlan = Planner.Plan();
                    }
                    else
                    {
                        if (highLevelPlanerType == HighLevelPlanerType.PDB)
                        {
                            DateTime startPdbCreation = DateTime.Now;
                            pdb = new PatternDatabase(lDomains, lProblems, agents, pdbPath);
                            pdbCreationTime = DateTime.Now.Subtract(startPdbCreation).TotalSeconds;
                            foreach (Agent agent in agents)
                            {
                                agent.InitMutex();
                            }
                            Distributed_Landmarks_Detection.Reset(agents);
                            List<Landmark> PublicAndArtificialGoals = FindPublicAndArtificialGoals(agents);

                            PdbPlaner Planner = new PdbPlaner(agents, PublicAndArtificialGoals, pdb);
                            Console.WriteLine("Planning");

                            lPlan = Planner.Plan();

                        }
                        else
                        {
                            if (highLevelPlanerType == HighLevelPlanerType.ForwardHsp)
                            {
                                bool stop = false;
                                while (!stop)
                                {
                                    stop = true;
                                    string name = "";
                                    GroundedPredicate currentGoal = null;
                                    foreach (Agent agent in agents)
                                    {
                                        currentGoal = agent.GetGoal();
                                        if (currentGoal != null)
                                        {
                                            stop = false;
                                            name = agent.name;
                                            break;
                                        }
                                    }
                                    if (!stop)
                                    {
                                        foreach (Agent agent in agents)
                                        {
                                            if (!agent.name.Equals(name))
                                            {
                                                agent.ReceiveGoal(currentGoal);
                                            }
                                        }
                                    }

                                }
                                foreach (Agent agent in agents)
                                {
                                    agent.InitMutex();
                                }
                                Distributed_Landmarks_Detection.Reset(agents);
                                PlanerHsp Planner = new PlanerHsp(agents);
                                Console.WriteLine("Planning");
                                lPlan = Planner.Plan();
                            }
                            else
                            {
                                if (highLevelPlanerType == HighLevelPlanerType.BackwardHsp)
                                {
                                    bool stop = false;
                                    List<GroundedPredicate> publishPublic = new List<GroundedPredicate>();
                                    List<GroundedPredicate> nextPublishPublic = new List<GroundedPredicate>();
                                    foreach (Agent a in agents)
                                    {
                                        publishPublic.AddRange(a.InitBackwardHspGraph());
                                    }
                                    bool outFlag = false;
                                    while (!stop)
                                    {
                                        stop = true;
                                        outFlag = false;
                                        foreach (Agent agent in agents)
                                        {
                                            nextPublishPublic.AddRange(agent.UpdateBackwardHspGraph(publishPublic, out outFlag));
                                            if (outFlag)
                                            {
                                                stop = false;
                                            }
                                        }
                                        publishPublic = nextPublishPublic;
                                    }

                                    foreach (Agent agent in agents)
                                    {
                                        agent.InitMutex();
                                    }
                                    Distributed_Landmarks_Detection.Reset(agents);
                                    // agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents);
                                    PlanerHspII Planner = new PlanerHspII(agents);
                                    Console.WriteLine("Planning");
                                    lPlan = Planner.Plan();

                                }
                                else
                                {
                                    if (highLevelPlanerType == HighLevelPlanerType.LandmarkAndHsp)
                                    {
                                        bool stop = false;
                                        while (!stop)
                                        {
                                            stop = true;
                                            string name = "";
                                            GroundedPredicate currentGoal = null;
                                            foreach (Agent agent in agents)
                                            {
                                                currentGoal = agent.GetGoal();
                                                if (currentGoal != null)
                                                {
                                                    stop = false;
                                                    name = agent.name;
                                                    break;
                                                }
                                            }
                                            if (!stop)
                                            {
                                                foreach (Agent agent in agents)
                                                {
                                                    if (!agent.name.Equals(name))
                                                    {
                                                        agent.ReceiveGoal(currentGoal);
                                                    }
                                                }
                                            }

                                        }
                                        foreach (Agent agent in agents)
                                        {
                                            agent.InitMutex();
                                        }

                                        agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);

                                        PlanerHspAndLandmarks Planner = new PlanerHspAndLandmarks(agents);

                                        Console.WriteLine("Planning..");

                                        lPlan = Planner.Plan();
                                    }
                                    else
                                    {
                                        if (highLevelPlanerType == HighLevelPlanerType.WeightedLandmarkAndHsp)
                                        {
                                            bool stop = false;
                                            while (!stop)
                                            {
                                                stop = true;
                                                string name = "";
                                                GroundedPredicate currentGoal = null;
                                                foreach (Agent agent in agents)
                                                {
                                                    currentGoal = agent.GetGoal();
                                                    if (currentGoal != null)
                                                    {
                                                        stop = false;
                                                        name = agent.name;
                                                        break;
                                                    }
                                                }
                                                if (!stop)
                                                {
                                                    foreach (Agent agent in agents)
                                                    {
                                                        if (!agent.name.Equals(name))
                                                        {
                                                            agent.ReceiveGoal(currentGoal);
                                                        }
                                                    }
                                                }

                                            }
                                            foreach (Agent agent in agents)
                                            {
                                                agent.InitMutex();
                                            }
                                            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);
                                            PlanerWeightedLandmarkAndHsp Planner = new PlanerWeightedLandmarkAndHsp(agents);
                                            Console.WriteLine("Planning");
                                            lPlan = Planner.Plan();
                                        }
                                        else
                                        {
                                            if (highLevelPlanerType == HighLevelPlanerType.SophisticatedProjection)
                                            {
                                                bool stop = false;
                                                while (!stop)
                                                {
                                                    stop = true;
                                                    string name = "";
                                                    GroundedPredicate currentGoal = null;
                                                    foreach (Agent agent in agents)
                                                    {
                                                        currentGoal = agent.GetGoal();
                                                        if (currentGoal != null)
                                                        {
                                                            stop = false;
                                                            name = agent.name;
                                                            break;
                                                        }
                                                    }
                                                    if (!stop)
                                                    {
                                                        foreach (Agent agent in agents)
                                                        {
                                                            if (!agent.name.Equals(name))
                                                            {
                                                                agent.ReceiveGoal(currentGoal);
                                                            }
                                                        }
                                                    }

                                                }
                                                foreach (Agent agent in agents)
                                                {
                                                    agent.InitMutex();
                                                }
                                              //  agents = AdvancedLandmarkProjectionAgents.CreateProjAgents(agents, lDomains, lProblems);

                                                agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);
                                                PlanerHspAndLandmarks Planner = new PlanerHspAndLandmarks(agents);
                                                Console.WriteLine("Planning");
                                                lPlan = Planner.Plan();
                                            }
                                            else
                                            {
                                                if (highLevelPlanerType == HighLevelPlanerType.PDBMafs)
                                                {
                                                    MafsVertex.pdb = new PatternDatabase(lDomains, lProblems, agents, pdbPath);
                                                    bool stop = false;
                                                    while (!stop)
                                                    {
                                                        stop = true;
                                                        string name = "";
                                                        GroundedPredicate currentGoal = null;
                                                        foreach (Agent agent in agents)
                                                        {
                                                            currentGoal = agent.GetGoal();
                                                            if (currentGoal != null)
                                                            {
                                                                stop = false;
                                                                name = agent.name;
                                                                break;
                                                            }
                                                        }
                                                        if (!stop)
                                                        {
                                                            foreach (Agent agent in agents)
                                                            {
                                                                if (!agent.name.Equals(name))
                                                                {
                                                                    agent.ReceiveGoal(currentGoal);
                                                                }
                                                            }
                                                        }

                                                    }
                                                    foreach (Agent agent in agents)
                                                    {
                                                        agent.InitMutex();
                                                    }


                                                    Console.WriteLine("Planning");

                                                    ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, lDomains, lProblems);

                                                    StartGrounding = DateTime.Now;
                                                    lPlan = Planner.Plan();

                                                }
                                                else
                                               if (highLevelPlanerType == HighLevelPlanerType.MafsLandmark)
                                                {
                                                    bool stop = false;
                                                    while (!stop)
                                                    {
                                                        stop = true;
                                                        string name = "";
                                                        GroundedPredicate currentGoal = null;
                                                        foreach (Agent agent in agents)
                                                        {
                                                            currentGoal = agent.GetGoal();
                                                            if (currentGoal != null)
                                                            {
                                                                stop = false;
                                                                name = agent.name;
                                                                break;
                                                            }
                                                        }
                                                        if (!stop)
                                                        {
                                                            foreach (Agent agent in agents)
                                                            {
                                                                if (!agent.name.Equals(name))
                                                                {
                                                                    agent.ReceiveGoal(currentGoal);
                                                                }
                                                            }
                                                        }

                                                    }
                                                    foreach (Agent agent in agents)
                                                    {
                                                        agent.InitMutex();
                                                    }

                                                    agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

                                                    Console.WriteLine("Planning");

                                                    ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, lDomains, lProblems);

                                                    StartGrounding = DateTime.Now;
                                                    lPlan = Planner.Plan();

                                                }
                                                else
                                               if (highLevelPlanerType == HighLevelPlanerType.Mafsff)
                                                {
                                                    bool stop = false;
                                                    while (!stop)
                                                    {
                                                        stop = true;
                                                        string name = "";
                                                        GroundedPredicate currentGoal = null;
                                                        foreach (Agent agent in agents)
                                                        {
                                                            currentGoal = agent.GetGoal();
                                                            if (currentGoal != null)
                                                            {
                                                                stop = false;
                                                                name = agent.name;
                                                                break;
                                                            }
                                                        }
                                                        if (!stop)
                                                        {
                                                            foreach (Agent agent in agents)
                                                            {
                                                                if (!agent.name.Equals(name))
                                                                {
                                                                    agent.ReceiveGoal(currentGoal);
                                                                }
                                                            }
                                                        }

                                                    }
                                                    foreach (Agent agent in agents)
                                                    {
                                                        agent.InitMutex();
                                                    }

                                                    agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

                                                    Console.WriteLine("Planning");

                                                    ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, lDomains, lProblems);

                                                    StartGrounding = DateTime.Now;
                                                    lPlan = Planner.PreferableFFPlan();

                                                }
                                                else                                                
                                                {
                                                    if (highLevelPlanerType == HighLevelPlanerType.ProjectionMafs)
                                                    {
                                                        bool stop = false;
                                                        while (!stop)
                                                        {
                                                            stop = true;
                                                            string name = "";
                                                            GroundedPredicate currentGoal = null;
                                                            foreach (Agent agent in agents)
                                                            {
                                                                currentGoal = agent.GetGoal();
                                                                if (currentGoal != null)
                                                                {
                                                                    stop = false;
                                                                    name = agent.name;
                                                                    break;
                                                                }
                                                            }
                                                            if (!stop)
                                                            {
                                                                foreach (Agent agent in agents)
                                                                {
                                                                    if (!agent.name.Equals(name))
                                                                    {
                                                                        agent.ReceiveGoal(currentGoal);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                        foreach (Agent agent in agents)
                                                        {
                                                            agent.InitMutex();
                                                        }

                                                        agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

                                                        Console.WriteLine("Planning");

                                                        ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, lDomains, lProblems);

                                                        StartGrounding = DateTime.Now;
                                                        lPlan = Planner.PreferablePlan();

                                                    }
                                                    else
                                                    if (highLevelPlanerType == HighLevelPlanerType.DistrebutedProjectionMafs)
                                                    {
                                                        bool stop = false;
                                                        while (!stop)
                                                        {
                                                            stop = true;
                                                            string name = "";
                                                            GroundedPredicate currentGoal = null;
                                                            foreach (Agent agent in agents)
                                                            {
                                                                currentGoal = agent.GetGoal();
                                                                if (currentGoal != null)
                                                                {
                                                                    stop = false;
                                                                    name = agent.name;
                                                                    break;
                                                                }
                                                            }
                                                            if (!stop)
                                                            {
                                                                foreach (Agent agent in agents)
                                                                {
                                                                    if (!agent.name.Equals(name))
                                                                    {
                                                                        agent.ReceiveGoal(currentGoal);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                        foreach (Agent agent in agents)
                                                        {
                                                            agent.InitMutex();
                                                        }

                                                        agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

                                                        Console.WriteLine("Planning");

                                                        ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, lDomains, lProblems);

                                                        StartGrounding = DateTime.Now;
                                                        lPlan = Planner.DistrebutedPreferablePlan();

                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("highLevelPlanerType did not selected");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    KillPlanners();
                    return null;
                }
            }
            End = DateTime.Now;
            KillPlanners();
            m_agents = agents;
            return lPlan;
        }

        /// <summary>
        /// Iterate over the agents domain and ground them
        /// </summary>
        /// <param name="lDomains">The view each agent has of the domain</param>
        /// <param name="lProblems">The view each agent has of the problem</param>
        /// <returns>The view for each agent has of the grounded domain.</returns>
        private static List<Domain> GroundDomains(List<Domain> lDomains, List<Problem> lProblems)
        {
            bool bNewPublicFacts = true;
            HashSet<GroundedPredicate> lPublicFacts = new HashSet<GroundedPredicate>();
            for (int i = 0; i < lDomains.Count; i++)
            {
                lDomains[i].StartGrounding(lProblems[i], lPublicFacts);
            }

            while (bNewPublicFacts)
            {
                int cPublicFacts = lPublicFacts.Count;
                for (int i = 0; i < lDomains.Count; i++)
                {
                    lDomains[i].ContinueGrounding(lProblems[i], lPublicFacts);

                }
                bNewPublicFacts = lPublicFacts.Count > cPublicFacts;
            }
            List<Domain> lGrounded = new List<Domain>();
            for (int i = 0; i < lDomains.Count; i++)
            {
                lGrounded.Add(lDomains[i].FinishGrounding(i));
            }
            return lGrounded;
        }

        public static List<Landmark> FindPublicAndArtificialGoals(List<Agent> agents)
        {
            List<Landmark> PublicAndArtificialGoals = new List<Landmark>();
            foreach (Agent agent in agents)
            {
                List<Landmark> agentsGoal = agent.GetPublicAndArtificialGoal();

                for (int i = 0; i < agentsGoal.Count; i++)
                {
                    Landmark newGoal = agentsGoal.ElementAt(i);
                    if (!PublicAndArtificialGoals.Contains(newGoal, new LandmarkEqualityComparer()))
                    {
                        PublicAndArtificialGoals.Add(newGoal);
                    }
                }
            }
            return PublicAndArtificialGoals;
        }

        private static List<string> SolveUsingProjectedPublicActions(List<Domain> lGrounded, List<Problem> lProblems)
        {
            Console.WriteLine("Attempting solving using public actions only");
            List<Action> lAllPublicActions = new List<Action>();
            foreach (Domain d in lGrounded)
                lAllPublicActions.AddRange(d.GetProjectedPublicActions());
            Domain dJoint = lGrounded[0].Clone();
            dJoint.Actions = lAllPublicActions;
            Problem pJoint = lProblems[0].Clone();
            pJoint.Domain = dJoint;
            //BUGBUG - may need to copy all initial public predicates to the joint problem
            bool bUnsolvable = false;
            State s = new State(pJoint);
            foreach (GroundedPredicate gp in pJoint.Known)
                s.AddPredicate(gp);
            ExternalPlanners externalPlanners = new ExternalPlanners();
            List<string> lPlan = externalPlanners.FFPlan(dJoint, pJoint, s, null, null, 2 * 60 * 1000, out bUnsolvable);
            if (bUnsolvable)
                Console.WriteLine("Cannot solve the problem using public actions only");
            else if (lPlan != null)
            {
                Console.WriteLine("Solved by public actions only");
                foreach (string sAction in lPlan)
                    Domain.MapGroundedActionNamesToOutputNames[sAction] = "(" + sAction + ")";
                return lPlan;
            }
            return null;
        }

        private static List<string> SolveSingleAgent(List<Domain> lDomains, List<Problem> lProblems)
        {
            Console.WriteLine("Attempting single agent hacks");

            List<string> lPlan = null;
            for (int i = 0; i < lDomains.Count; i++)
            {
                State s = new State(lProblems[i]);
                foreach (GroundedPredicate gp in lProblems[i].Known)
                    s.AddPredicate(gp);
                bool bUnsolvable = false;
                ExternalPlanners externalPlanners = new ExternalPlanners();
                lPlan = externalPlanners.FFPlan(lDomains[i], lProblems[i], s, null, null, 2 * 60 * 1000, out bUnsolvable);
                if (bUnsolvable)
                    Console.WriteLine("Agent " + i + " cannot solve the problem alone");
                else if (lPlan != null)
                {
                    Console.WriteLine("Solved by agent " + i);
                    foreach (string sAction in lPlan)
                        Domain.MapGroundedActionNamesToOutputNames[sAction] = "(" + sAction + ")";
                    return lPlan;
                }
            }
            return null;
        }

        public static void WritePlanToFile(List<string> lPlan, string sFileName)
        {

            StreamWriter swOutput = new StreamWriter(Directory.GetCurrentDirectory() + @"/" + sFileName);

            if (lPlan != null)
            {
                for (int i = 0; i < lPlan.Count; i++)
                {
                    string sOutputName = Domain.MapGroundedActionNamesToOutputNames[lPlan[i]];
                    swOutput.WriteLine(i + " : " + sOutputName);
                    Console.WriteLine(i + " : " + sOutputName);
                }
            }
            swOutput.Close();

        }

        public static bool CanDo(Action act, HashSet<GroundedPredicate> set)
        {
            bool f = true;
            foreach (GroundedPredicate pre in act.HashPrecondition)
            {
                if (!set.Contains(pre))
                {
                    f = false;
                    break;
                }
            }
            return f;
        }


        /// <summary>
        /// Run the designated planner on all the problem instances in the given input directory and its subdirectories 
        /// </summary>
        /// <param name="inputDirectory">The directory where the problem instances to parse are located at </param>
        /// <param name="outputPlanFile">The name of the file where the generated plan is written to</param>
        private static void ParseAndSolve(DirectoryInfo inputDirectory, string outputPlanFile)
        {
            Start = new DateTime(0);
            StartHighLevelPlanning = new DateTime(0);
            StartGrounding = new DateTime(0);
            End = new DateTime(0);
            PlanCost = -1;
            PlanMakeSpan = -1;

            Console.WriteLine("Scanning directory " + inputDirectory.Name);
            bool hasRealSubdirs = false;
            if (inputDirectory.GetDirectories().Length != 0)
            {
                foreach (DirectoryInfo inputSubdir in inputDirectory.GetDirectories().OrderBy(dir => dir.Name.ToString()))
                {
                    if (!inputSubdir.Name.ToLower().Contains("pdb"))
                    {
                        // Recursive call, to search for the leaves of the current directory tree that contains problem instances
                        ParseAndSolve(inputSubdir, outputPlanFile);
                        hasRealSubdirs = true;
                    }
                }
            }
            if (!hasRealSubdirs)
            {
                // Run planner on the problem instances in the current directory

                if (inputDirectory.ToString().Contains("PdbFiles"))
                    return;
   
                if (outputPlanFile == "")
                    outputPlanFile = "Plan.txt";

                Vertex.agents = new List<Agent>();
                Vertex.ffLplan = new List<string>();
                Vertex.map = new Dictionary<string, int>();
                Vertex.forwardSearch = null;
                Vertex.hsp = null;

                GC.Collect();



                // Run the planner on a separate thread
                Thread planningThread = new Thread(() => RunExperiment(inputDirectory, outputPlanFile));
                planningThread.Name = "ReadAgentFiles " + inputDirectory.Name;
                planningThread.Start();

                // Wait until the planning thread is done
                if (planningThread.Join(TIMEOUT_PER_INSTANCE))
                { // Planning has finished successfully before timeout
                }
                else
                {
                    // Timeout reached, so abort the planning thre, print an empty plan, and report timeout
                    planningThread.Abort();
                    Thread.Sleep(1000);
                    
                    //writing an empty plan file
                    StreamWriter sw = new StreamWriter(outputPlanFile);
                    sw.Close();

                    WriteResults(inputDirectory.Name, "failed - timeout");
                }

                // Kill threads 
                KillPlanners();
            }
        }

        /// <summary>
        /// Get all processes that we think are planners
        /// <todo>HAVE A LIST OF PLANNER PROCESS NAMES TO READ FROM</todo>
        /// </summary>
        /// <returns></returns>
        public static List<Process> GetPlanningProcesses()
        {
            List<Process> processList = new List<Process>();
            foreach (Process process in Process.GetProcesses())
            {
                if (process.ProcessName.ToLower().Contains("downward") || process.ProcessName.ToLower().Contains("ff"))
                    processList.Add(process);
            }
            return processList;
        }

        public static void KillAll(List<Process> l)
        {

            foreach (Process p in l)
            {
                try
                {
                    if (!p.HasExited)
                    {

                        p.Kill();
                        Thread.Sleep(40);
                        // p.WaitForExit();
                        p.Close();


                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine("*");
                }


            }


        }
        private static void RunUsingProcesses(DirectoryInfo di, string sOutputPlanFile)
        {
            Start = new DateTime(0);
            StartHighLevelPlanning = new DateTime(0);
            StartGrounding = new DateTime(0);
            End = new DateTime(0);
            PlanCost = -1;
            PlanMakeSpan = -1;

            Console.WriteLine("Scanning directory " + di.Name);
            bool bContainsRealDirectories = false;
            int cSubs = 0;
            if (di.GetDirectories().Length != 0)
            {
                foreach (DirectoryInfo diSub in di.GetDirectories().OrderBy(d => d.Name.ToString()))
                {
                    if (!diSub.Name.ToLower().Contains("pdb"))
                    {
                        RunUsingProcesses(diSub, sOutputPlanFile);
                        bContainsRealDirectories = true;
                        cSubs++;
                    }
                }
            }
            if (!bContainsRealDirectories)
            {
                if (di.ToString().Contains("PdbFiles"))
                    return;

                Process p = new Process();
                p.StartInfo.FileName = "GPPP.exe";

                p.StartInfo.Arguments = "\"" + di.FullName + "\"";// "\"" + +"\",\"" + sOutputPlanFile + "\"";
                p.Start();
                if (!p.WaitForExit(30 * 60 * 1000))
                {
                    p.Kill();
                    p.WaitForExit();
                }
                KillPlanners();
            }
        }


        /// <summary>
        /// Kills all the planning processes
        /// </summary>
        public static void KillPlanners()
        {
            List<Process> lPlanningProcesses = GetPlanningProcesses();
            KillAll(lPlanningProcesses);
        }

        public static int ffMessageCounter = 0;


        /// <summary>
        /// Write all the results of a single run to an output file
        /// </summary>
        /// <param name="sDomain"></param>
        /// <param name="sMsg"></param>
        public static void WriteResults(string sDomain, string sMsg, Boolean append=true)
        {
            Console.WriteLine(sDomain + " " + sMsg);
            swResults = new StreamWriter(@"Results.txt", append);
            swResults.WriteLine(sDomain + ", " + sMsg.Replace(",", ":") + ", " + PlanCost + ", " + PlanMakeSpan
               + "," + (End.Subtract(Start).TotalSeconds - pdbCreationTime)
               + "," + sendedStateCounter
               + "," + StateExpendCounter
               + "," + ImprovedMafsPlanner.generateCounter
               + "," + ffMessageCounter
            );
            swResults.Close();
            Console.WriteLine("Time: " + (End.Subtract(Start).TotalSeconds - pdbCreationTime));
        }


        public static void CreateMABlocksWorld(string sPath, int cMinArms, int cMaxArms, int cMinStacksPerArm, int cMaxStacksPerArm, int cMinBlocks, int cMaxBlocks, int cBlocksStep)
        {
            for (int cArms = cMinArms; cArms <= cMaxArms; cArms += 1)
            {
                for (int cStacks = cMinStacksPerArm; cStacks <= cMaxStacksPerArm; cStacks += 1)
                {
                    for (int cBlocks = cMinBlocks; cBlocks <= cMaxBlocks; cBlocks += cBlocksStep)
                    {
                        MABlocksWorld2 world = new MABlocksWorld2(cBlocks, cArms, cStacks);
                        world.WriteFactoredDomains(sPath + "/" + world.Name);
                        world.WriteFactoredProblems(sPath + "/" + world.Name);
                    }

                }
            }
        }
        public static void CreateMALogistics(string sPath, int cMinAreas, int cMaxAreas, int cMinCitiesPerArea, int cMaxCitiesPerArea, int cMincPackages, int cMaxcPackages, int cBlocksStep)
        {
            for (int cAreas = cMinAreas; cAreas <= cMaxAreas; cAreas += 1)
            {
                for (int cCitiesPerArea = cMinCitiesPerArea; cCitiesPerArea <= cMaxCitiesPerArea; cCitiesPerArea += 1)
                {
                    for (int cPackages = cMincPackages; cPackages <= cMaxcPackages; cPackages += cBlocksStep)
                    {
                        MALogistics world = new MALogistics(cPackages, cAreas, cCitiesPerArea);
                        world.WriteFactoredDomains(sPath + "/" + world.Name);
                        world.WriteFactoredProblems(sPath + "/" + world.Name);
                    }

                }
            }
        }
        static StreamWriter swResults;
        

        
        /**
         * The main function that runs the planner.
         **/ 
        static void Main(string[] args)
        {
            //string problemsInputDir = @"..\..\..\..\factored-domains\blocksworld"; // Default problem set
            string problemsInputDir = @"C:\projects\MAP\factored-domains\blocksworld\probBLOCKS-simple"; // Default problem set

            
            string planOutputFile = "Plan.txt"; // Default file where to write the generated plan
            Console.WriteLine("Running configuration " + highLevelPlanerType);

            // Default running configuration
            if (args.Length >= 1)
            {
                problemsInputDir = args[0];
            }
            if (args.Length == 2)
            {
                planOutputFile = args[1];
            }

            // Go over all the problem instance files in the input directory, parse them, and solve them. 
            ParseAndSolve(new DirectoryInfo(problemsInputDir), planOutputFile);
        }
    }
}
