﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Planning.planners;

namespace Planning
{
    class ProblemSolver
    {
        private enum HighLevelPlanerType { PDB, Landmark, Projection, ForwardHsp, BackwardHsp, LandmarkAndHsp, WeightedLandmarkAndHsp, SophisticatedProjection, MafsLandmark, Mafsff, MafsWithProjectionLandmarks, PDBMafs, ProjectionMafs, DistrebutedProjectionMafs };
        private HighLevelPlanerType highLevelPlanerType = HighLevelPlanerType.LandmarkAndHsp;
        private enum ProjectionVersion { Local, Global, GlobalV2, GlobalWithMemo, fullGlobal, ProjectionFF, NULL };
        private ProjectionVersion projectionVersion = ProjectionVersion.ProjectionFF;
        private DateTime Start, StartHighLevelPlanning, StartGrounding, End;
        private double pdbCreationTime;
        private enum PlanerType { ff_tryCoordinate, hsp_tryCoordinate, ff_directPlan, hsp_directPlan, ff_toActions };
        private PlanerType internalPlaner = PlanerType.ff_toActions;


        private PatternDatabase pdb = null; // Not the best design choice to leave this here. <todo> move to the planner code </todo>

        /// <summary>
        /// Setup internal variables
        /// </summary>
        private void Setup()
        {
            if (highLevelPlanerType != HighLevelPlanerType.ProjectionMafs)
            {
                projectionVersion = ProjectionVersion.NULL;
            }
            Start = DateTime.Now;
            pdbCreationTime = 0;
        }

        /// <summary>
        /// Iterate over the agents domain and ground them
        /// </summary>
        /// <param name="domains">The view each agent has of the domain</param>
        /// <param name="problems">The view each agent has of the problem</param>
        /// <returns>The view for each agent has of the grounded domain.</returns>
        public List<Domain> GroundDomains(List<Domain> domains, List<Problem> problems)
        {
            bool bNewPublicFacts = true;
            HashSet<GroundedPredicate> publicFacts = new HashSet<GroundedPredicate>();
            for (int i = 0; i < domains.Count; i++)
            {
                domains[i].StartGrounding(problems[i], publicFacts);
            }

            while (bNewPublicFacts)
            {
                int cPublicFacts = publicFacts.Count;
                for (int i = 0; i < domains.Count; i++)
                {
                    domains[i].ContinueGrounding(problems[i], publicFacts);

                }
                bNewPublicFacts = publicFacts.Count > cPublicFacts;
            }
            List<Domain> grounded = new List<Domain>();
            for (int i = 0; i < domains.Count; i++)
                grounded.Add(domains[i].FinishGrounding(i));

            // Update the public/private action list of the agents (<todo> reconsider if this is the correct place for this function</todo>)
            foreach (Domain groundedDomain in grounded)
                groundedDomain.InitializePublicAndPrivateActions();

            return grounded;
        }

        public List<string> SolveFactored(MaStripsProblem maStripsProblem)
        {
            List<Domain> lDomains=maStripsProblem.domains;
            List<Problem> lProblems=maStripsProblem.problems;
            
            PlanningLogger.Log("Grounding");
            List<Domain> lGrounded = GroundDomains(lDomains, lProblems);
            MaStripsProblem groundedMaStripProble = new MaStripsProblem(lGrounded, lProblems, maStripsProblem.inputDirectory);

            //BuildAgents.mapActionNameToAgents = new Dictionary<string, HashSet<string>>();
            //List<Domain> lGrounded = GroundDomains(lDomains, lProblems);
            //BuildAgents buildAgents = new BuildAgents(lDomains, lGrounded, lProblems);
            //List<Agent> agents = buildAgents.ReturnAgents();

            // Extract for each agent the public predicates that affect it (i.e., is a precondition or effects of its actions)
            //Dictionary<string, HashSet<GroundedPredicate>> allPublicPredicate  
            //                = ExtractPublicPredicates(agents);
            

            StartHighLevelPlanning = DateTime.Now;

            AbstractPlanner planner = PlannerFactory.GetInstance().CreateSimpleMafsPlanner(this);
            //List<string> lPlan = planner.Plan(maStripsProblem, ref agents);
            List<string> lPlan = planner.Plan(groundedMaStripProble);

            //List<string> lPlan = Plan(maStripsProblem, ref agents);           
            End = DateTime.Now;
            return lPlan;
        }


        /// <summary>
        /// Run an MA-STRIPS planner to solve the given MA-STRIPS problem
        /// </summary>
        /// <param name="maStripsProblem"></param>
        /// <param name="agents"></param>
        /// <returns></returns>
        private List<string> Plan(MaStripsProblem maStripsProblem, ref List<Agent> agents)
        {
            List<string> lPlan=null;
            if (highLevelPlanerType == HighLevelPlanerType.Projection)
            {
                lPlan = HighLevelPlannerProjection(maStripsProblem, agents);
            }
            else
            {
                if (highLevelPlanerType == HighLevelPlanerType.Landmark)
                {
                    lPlan = HighLevelPlannerLandmark(ref agents);
                }
                else
                {
                    if (highLevelPlanerType == HighLevelPlanerType.PDB)
                    {
                        lPlan = HighLevelPlannerPDB(maStripsProblem, agents);
                    }
                    else
                    {
                        if (highLevelPlanerType == HighLevelPlanerType.ForwardHsp)
                        {
                            lPlan = HighLevelPlannerForwardHsp(agents);
                        }
                        else
                        {
                            if (highLevelPlanerType == HighLevelPlanerType.BackwardHsp)
                            {
                                lPlan = HighLevelPlanerBackwardHsp(agents);
                            }
                            else
                            {
                                if (highLevelPlanerType == HighLevelPlanerType.LandmarkAndHsp)
                                {
                                    lPlan = HighLevelPlanerLandmarkAndHsp(ref agents);
                                }
                                else
                                {
                                    if (highLevelPlanerType == HighLevelPlanerType.WeightedLandmarkAndHsp)
                                    {
                                        lPlan = HighLevelPlanerWeightedLandmarkAndHsp(ref agents);
                                    }
                                    else
                                    {
                                        if (highLevelPlanerType == HighLevelPlanerType.SophisticatedProjection)
                                        {
                                            lPlan = HighLevelPlanerSophisticatedProjection(ref agents);
                                        }
                                        else
                                        {
                                            if (highLevelPlanerType == HighLevelPlanerType.PDBMafs)
                                            {
                                                lPlan = HighLevelPlanerPDBMafs(maStripsProblem, agents);
                                            }
                                            else
                                                if (highLevelPlanerType == HighLevelPlanerType.MafsLandmark)
                                                {
                                                    lPlan = HighLevelPlanerMafsLandmark(maStripsProblem, ref agents);
                                                }
                                                else
                                                    if (highLevelPlanerType == HighLevelPlanerType.Mafsff)
                                                    {
                                                        lPlan = HighLevelPlanerMafsff(maStripsProblem, ref agents);
                                                    }
                                                    else
                                                    {
                                                        if (highLevelPlanerType == HighLevelPlanerType.ProjectionMafs)
                                                        {
                                                            lPlan = HighLevelPlanerProjectionMafs(maStripsProblem, ref agents);
                                                        }
                                                        else
                                                            if (highLevelPlanerType == HighLevelPlanerType.DistrebutedProjectionMafs)
                                                            {
                                                                lPlan = HighLevelPlanerDistrebutedProjectionMafs(maStripsProblem, ref agents);
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("highLevelPlanerType did not selected");
                                                            }
                                                    }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            return lPlan;
        }




        private List<string> HighLevelPlanerDistrebutedProjectionMafs(MaStripsProblem maStripsProblem, ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

            Console.WriteLine("Planning");

            ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, maStripsProblem.domains, maStripsProblem.problems);

            StartGrounding = DateTime.Now;
            return Planner.DistrebutedPreferablePlan();
        }

        private List<string> HighLevelPlanerProjectionMafs(MaStripsProblem maStripsProblem, ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

            Console.WriteLine("Planning");

            ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, maStripsProblem.domains, maStripsProblem.problems);

            StartGrounding = DateTime.Now;
            return Planner.PreferablePlan();
        }

        private List<string> HighLevelPlanerMafsff(MaStripsProblem maStripsProblem, ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

            Console.WriteLine("Planning");

            ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, maStripsProblem.domains, maStripsProblem.problems);

            StartGrounding = DateTime.Now;
            return Planner.PreferableFFPlan();
        }

        private List<string> HighLevelPlanerMafsLandmark(MaStripsProblem maStripsProblem, ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, true);

            Console.WriteLine("Planning");

            ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, maStripsProblem.domains, maStripsProblem.problems);

            StartGrounding = DateTime.Now;
            return Planner.Plan();
        }

        private List<string> HighLevelPlanerPDBMafs(MaStripsProblem maStripsProblem, List<Agent> agents)
        {
            MafsVertex.pdb = new PatternDatabase(maStripsProblem.domains, maStripsProblem.problems, agents, maStripsProblem.GetPdbDirectory());
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }


            Console.WriteLine("Planning");

            ImprovedMafsPlanner Planner = new ImprovedMafsPlanner(agents, maStripsProblem.domains, maStripsProblem.problems);

            StartGrounding = DateTime.Now;
            return Planner.Plan();
        }

        private List<string> HighLevelPlanerSophisticatedProjection(ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }
            //  agents = AdvancedLandmarkProjectionAgents.CreateProjAgents(agents, lDomains, lProblems);

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);
            PlanerHspAndLandmarks Planner = new PlanerHspAndLandmarks(agents);
            Console.WriteLine("Planning");
            return Planner.Plan();
        }

        private List<string> HighLevelPlanerWeightedLandmarkAndHsp(ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }
            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);
            PlanerWeightedLandmarkAndHsp Planner = new PlanerWeightedLandmarkAndHsp(agents);
            Console.WriteLine("Planning");
            return Planner.Plan();
        }

        private List<string> HighLevelPlanerLandmarkAndHsp(ref List<Agent> agents)
        {
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);

            PlanerHspAndLandmarks Planner = new PlanerHspAndLandmarks(agents);

            Console.WriteLine("Planning..");

            return Planner.Plan();
        }

        private List<string> HighLevelPlanerBackwardHsp(List<Agent> agents)
        {
            bool stop = false;
            List<GroundedPredicate> publishPublic = new List<GroundedPredicate>();
            List<GroundedPredicate> nextPublishPublic = new List<GroundedPredicate>();
            foreach (Agent a in agents)
            {
                publishPublic.AddRange(a.InitBackwardHspGraph());
            }
            bool outFlag = false;
            while (!stop)
            {
                stop = true;
                outFlag = false;
                foreach (Agent agent in agents)
                {
                    nextPublishPublic.AddRange(agent.UpdateBackwardHspGraph(publishPublic, out outFlag));
                    if (outFlag)
                    {
                        stop = false;
                    }
                }
                publishPublic = nextPublishPublic;
            }

            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }
            Distributed_Landmarks_Detection.Reset(agents);
            // agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents);
            PlanerHspII Planner = new PlanerHspII(agents);
            Console.WriteLine("Planning");
            return Planner.Plan();
        }

        private List<string> HighLevelPlannerProjection(MaStripsProblem maStripsProblem, List<Agent> agents)
        {
            Console.WriteLine("Planning");
            PrepareAgentsGoals(agents);
            AdvancedLandmarkProjectionPlaner planner = new AdvancedLandmarkProjectionPlaner();
            return planner.Plan(agents, maStripsProblem.domains, maStripsProblem.problems, maStripsProblem.GetJointDomain());
        }

        private List<string> HighLevelPlannerForwardHsp(List<Agent> agents)
        {
            PrepareAgentsGoals(agents);


            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }
            Distributed_Landmarks_Detection.Reset(agents);
            PlanerHsp Planner = new PlanerHsp(agents);
            Console.WriteLine("Planning");
            return Planner.Plan();
        }

        /// <summary>
        /// To create list of landmarks that seed the search for additional landmarks, we use the public goals,
        /// and the public facts that are needed to achieve private goals.
        /// </summary>
        /// <param name="agents"></param>
        /// <returns></returns>
        private List<Landmark> FindPublicAndArtificialGoals(List<Agent> agents)
        {
            List<Landmark> PublicAndArtificialGoals = new List<Landmark>();
            foreach (Agent agent in agents)
            {
                List<Landmark> agentsGoal = agent.GetPublicAndArtificialGoal();

                for (int i = 0; i < agentsGoal.Count; i++)
                {
                    Landmark newGoal = agentsGoal.ElementAt(i);
                    if (!PublicAndArtificialGoals.Contains(newGoal, new LandmarkEqualityComparer()))
                    {
                        PublicAndArtificialGoals.Add(newGoal);
                    }
                }
            }
            return PublicAndArtificialGoals;
        }

        private List<string> HighLevelPlannerPDB(MaStripsProblem maStripsProblem, List<Agent> agents)
        {
            DateTime startPdbCreation = DateTime.Now;
            pdb = new PatternDatabase(maStripsProblem.domains, maStripsProblem.problems, agents, maStripsProblem.GetPdbDirectory());
            pdbCreationTime = DateTime.Now.Subtract(startPdbCreation).TotalSeconds;
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }
            Distributed_Landmarks_Detection.Reset(agents);
            List<Landmark> PublicAndArtificialGoals = FindPublicAndArtificialGoals(agents);

            PdbPlaner Planner = new PdbPlaner(agents, PublicAndArtificialGoals, pdb);
            Console.WriteLine("Planning");

            return Planner.Plan();
        }

        private List<string> HighLevelPlannerLandmark(ref List<Agent> agents)
        {
            Console.WriteLine("Identifying landmarks");
            PrepareAgentsGoals(agents);
            foreach (Agent agent in agents)
            {
                agent.InitMutex();
            }

            agents = Distributed_Landmarks_Detection.Landmarks_Detection(agents, false);
            Planer_I Planner = new Planer_I(agents);
            Console.WriteLine("Planning");
            return Planner.Plan();
        }


        /// <summary>
        /// Moving the public goals from some internal structure to another.
        /// <todo>Ask Shlomi why all this is needed</todo>
        /// </summary>
        /// <param name="agents"></param>
        private void PrepareAgentsGoals(List<Agent> agents)
        {
            bool stop = false;
            while (!stop)
            {
                stop = true;
                string name = "";
                GroundedPredicate currentGoal = null;
                foreach (Agent agent in agents)
                {
                    currentGoal = agent.GetGoal();
                    if (currentGoal != null)
                    {
                        stop = false;
                        name = agent.name;
                        break;
                    }
                }
                if (!stop)
                {
                    foreach (Agent agent in agents)
                    {
                        if (!agent.name.Equals(name))
                        {
                            agent.ReceiveGoal(currentGoal);
                        }
                    }
                }

            }
        }


        /// <summary>
        /// Extract for each agent the public predicates that 
        /// affect them, either as preconditions or as effects of their actions. 
        /// </summary>
        /// <param name="agents"></param>
        /// <returns>A mapping between an agent and the public predicates </returns>
        private Dictionary<string, HashSet<GroundedPredicate>> ExtractPublicPredicates(List<Agent> agents)
        {
            Dictionary<string, HashSet<GroundedPredicate>> allPublicPredicate 
                = new Dictionary<string, HashSet<GroundedPredicate>>();

            foreach (Agent agent in agents)
            {
                allPublicPredicate.Add(agent.name, new HashSet<GroundedPredicate>());
                foreach (Action publicAct in agent.publicActions)
                {
                    foreach (GroundedPredicate pre in publicAct.HashPrecondition)
                    {
                        if (agent.PublicPredicates.Contains(pre))
                        {
                            allPublicPredicate[agent.name].Add(pre);
                        }
                    }

                    foreach (GroundedPredicate eff in publicAct.HashEffects)
                    {
                        if (agent.PublicPredicates.Contains(eff))
                        {
                            allPublicPredicate[agent.name].Add(eff);
                        }
                    }
                }
            }
            return allPublicPredicate;
        }
    }
}
